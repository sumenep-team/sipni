<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Authentication Routes...
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

//// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');
//
//// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::prefix('sanggar')->middleware(['auth', 'isSanggar'])->group(function (){
    Route::get('dashboard', [App\Http\Controllers\Sanggar\DashboardController::class, 'index']);

    // Kelas routes
    Route::controller(\App\Http\Controllers\Sanggar\KelasController::class)->group(function () {
        Route::get('/kelas', 'index')->name('kelas'); //oke
        Route::get('/kelas/buat', 'create')->name('buat-kelas'); //oke
        Route::post('/kelas', 'store');
        Route::get('/kelas/{kelasId}/details', 'details')->name('edit-kelas');
        Route::get('/kelas/{kelas}/ubah', 'edit')->name('edit-kelas');//
        Route::put('/kelas/{kelas}', 'update');
    });

    // Kursus routes TODO: Buat Controller
    Route::controller(\App\Http\Controllers\Sanggar\KursusController::class)->group(function () {
        Route::get('/kursus', 'index')->name('kursusList');//oke
        Route::get('/kursus/buat', 'create'); //oke
        Route::post('/kursus', 'store');
        Route::get('/kursus/{id}/ubah', 'edit'); //oke
        Route::put('/kursus/{id}', 'update');
    });

});
//TODO: Buat namespace dan Controller
Route::prefix('pelajar')->middleware(['auth', 'isPelajar'])->group(function (){
    Route::get('dashboard', [App\Http\Controllers\Pelajar\DashboardController::class, 'index']);

    // Telusuri routes
    Route::controller(\App\Http\Controllers\Pelajar\TelusuriController::class)->group(function () {
        Route::get('/telusuri','index');
        Route::get('/sanggar/{sanggar_id}/daftarkelas','kelasOfSanggar');
        Route::get('/kelas/{kelas_id}','kelasDetails');
        Route::get('/detailsanggar/{sanggar_id}','sanggarDetails');

    });

    Route::controller(\App\Http\Controllers\Pelajar\WishlistController::class)->group(function () {
        Route::get('wishlist','index');
    });



});

