-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2023-06-21 05:29:02.448

-- tables
-- Table: jadwal
DROP TABLE IF EXISTS `jadwal`;
CREATE TABLE jadwal (
    id_jadwal varchar(36)  NOT NULL,
    hari tinyint  NOT NULL,
    jam_mulai time  NOT NULL,
    jam_selesai time  NULL,
    kelas_id_kelas varchar(36)  NOT NULL,
    CONSTRAINT jadwal_pk PRIMARY KEY (id_jadwal)
);

-- Table: kelas
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE kelas (
    id_kelas varchar(36)  NOT NULL,
    nama_kelas varchar(200)  NOT NULL,
    instruktur varchar(200)  NULL,
    status tinyint  NOT NULL,
    kapasitas int  NULL,
    jumlah_peserta int  NULL,
    harga int  NULL,
    kursus_id_kursus varchar(36)  NOT NULL,
    CONSTRAINT kelas_pk PRIMARY KEY (id_kelas)
);

-- Table: kelas_pelajar
DROP TABLE IF EXISTS `kelas_pelajar`;
CREATE TABLE kelas_pelajar (
    id_kelaspelajar varchar(36)  NOT NULL,
    no_ref_transfer varchar(100)  NOT NULL,
    pelajar_id_pelajar varchar(36)  NOT NULL,
    kelas_id_kelas varchar(36)  NOT NULL,
    CONSTRAINT kelas_pelajar_pk PRIMARY KEY (id_kelaspelajar)
);

-- Table: kursus
DROP TABLE IF EXISTS `kursus`;
CREATE TABLE kursus (
    id_kursus varchar(36)  NOT NULL,
    nama_kursus varchar(200)  NOT NULL,
    deskripsi varchar(255)  NULL,
    tempat varchar(255)  NULL,
    foto_kursus varchar(255)  NULL,
    sanggar_id_sanggar varchar(36)  NOT NULL,
    CONSTRAINT kursus_pk PRIMARY KEY (id_kursus)
);

-- Table: kursus_tag
DROP TABLE IF EXISTS `kursus_tag`;
CREATE TABLE kursus_tag (
    kursus_id_kursus varchar(36)  NOT NULL,
    tag_id_tag varchar(36)  NOT NULL,
    CONSTRAINT kursus_tag_pk PRIMARY KEY (kursus_id_kursus,tag_id_tag)
);

-- Table: pelajar
DROP TABLE IF EXISTS `pelajar`;
CREATE TABLE pelajar (
    id_pelajar varchar(36)  NOT NULL,
    username varchar(50)  NULL,
    nama varchar(200)  NOT NULL,
    deskripsi varchar(255)  NULL,
    kota varchar(200)  NULL,
    foto_profil varchar(255)  NULL,
    users_id_user varchar(36)  NOT NULL,
    CONSTRAINT pelajar_pk PRIMARY KEY (id_pelajar)
);

INSERT INTO `pelajar` (`id_pelajar`, `users_id_user`, `username`, `nama`, `deskripsi`, `kota`, `foto_profil`) VALUES
('3a892b48-0f77-11ee-8733-e8d8d14422c7', '5b77d34a-0f75-11ee-8733-e8d8d14422c7', 'awputra', 'Arya Widya Putra', NULL, 'Surabaya', NULL),
('3a895816-0f77-11ee-8733-e8d8d14422c7', '5b89cb72-0f75-11ee-8733-e8d8d14422c7', 'mfrParinduri', 'Mohammad Fadhil Rasyidin Parinduri', NULL, 'Surabaya', NULL),
('3a895a0c-0f77-11ee-8733-e8d8d14422c7', '5b89cc9a-0f75-11ee-8733-e8d8d14422c7', 'hfraryputra', 'Haidar Fico Ramadhan Aryputra', NULL, 'Surabaya', NULL);

-- Table: sanggar
DROP TABLE IF EXISTS `sanggar`;
CREATE TABLE sanggar (
    id_sanggar varchar(36)  NOT NULL,
    username varchar(50)  NULL,
    nama varchar(200)  NOT NULL,
    deskripsi varchar(255)  NULL,
    kota varchar(200)  NULL,
    no_rek varchar(50)  NULL,
    foto_profil varchar(255)  NULL,
    users_id_user varchar(36)  NOT NULL,
    CONSTRAINT sanggar_pk PRIMARY KEY (id_sanggar)
);

INSERT INTO `sanggar` (`id_sanggar`, `users_id_user`, `username`, `nama`, `deskripsi`, `kota`, `no_rek`, `foto_profil`) VALUES
('1c308c7e-0f78-11ee-8733-e8d8d14422c7', 'ae263aa2-0f77-11ee-8733-e8d8d14422c7', 'wiraga_ayu', 'Sanggar Wiraga Ayu', NULL, 'Surabaya', '2910117354', NULL),
('1c309d5a-0f78-11ee-8733-e8d8d14422c7', 'ae264ae2-0f77-11ee-8733-e8d8d14422c7', 'namarina', 'Sanggar Namarina', NULL, 'Surabaya', '5410425787', NULL);

-- Table: tag
DROP TABLE IF EXISTS `tag`;
CREATE TABLE tag (
    id_tag varchar(36)  NOT NULL,
    nama_tag varchar(200)  NOT NULL,
    CONSTRAINT tag_pk PRIMARY KEY (id_tag)
);

-- Table: users
DROP TABLE IF EXISTS `users`;
CREATE TABLE users (
    id_user varchar(36)  NOT NULL,
    nama varchar(200)  NOT NULL,
    email varchar(200)  NOT NULL,
    password varchar(60)  NOT NULL,
    `group` varchar(20)  NOT NULL,
    lang boolean  NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY (id_user)
);

INSERT INTO `users` (`id_user`, `nama`, `email`, `password`, `group`, `lang`) VALUES
('5b77d34a-0f75-11ee-8733-e8d8d14422c7', 'Arya Widya Putra', 'emailarya@gmail.com', '$2y$10$fcVNNRvX90fDCricvkoyPuNCoFhIFi.TQwhIu5lsoUqu0MsV9yTKC', 'pelajar', 0),
('5b89cb72-0f75-11ee-8733-e8d8d14422c7', 'Mohammad Fadhil Rasyidin Parinduri', 'emailfadhil@gmail.com', '$2y$10$nP/n7Hh9T13udx0b6PAjNOdz7e2bOrpRb6dTimRxAPcHD/FoXFxVG', 'pelajar', 0),
('5b89cc9a-0f75-11ee-8733-e8d8d14422c7', 'Haidar Fico Ramadhan Aryputra', 'emailfico@gmail.com', '$2y$10$Os9Nh0YqPfTJ.LZtdjOUceappeKDLdfk8tKgW/eUnccpKic2BuPPm', 'pelajar', 0),
('ae263aa2-0f77-11ee-8733-e8d8d14422c7', 'Sanggar Wiraga Ayu', 'wiragaayu@gmail.com', '$2y$10$neVGEHQNyFNL9IR3xJYNjuoBALsXPFpaiaACnAMkwWXXsmbDsqQx6', 'sanggar', 0),
('ae264ae2-0f77-11ee-8733-e8d8d14422c7', 'Sanggar Namarina', 'namarina@gmail.com', '$2y$10$st9GQGBWNai3ftahPW4tveibqU1e0CMUfdtMp.UwQ8l.4j6KVbBB2', 'sanggar', 0);

-- foreign keys
-- Reference: jadwal_kelas (table: jadwal)
ALTER TABLE jadwal ADD CONSTRAINT jadwal_kelas FOREIGN KEY jadwal_kelas (kelas_id_kelas)
    REFERENCES kelas (id_kelas);

-- Reference: kelas_kursus (table: kelas)
ALTER TABLE kelas ADD CONSTRAINT kelas_kursus FOREIGN KEY kelas_kursus (kursus_id_kursus)
    REFERENCES kursus (id_kursus);

-- Reference: kelaspelajar_kelas (table: kelas_pelajar)
ALTER TABLE kelas_pelajar ADD CONSTRAINT kelaspelajar_kelas FOREIGN KEY kelaspelajar_kelas (kelas_id_kelas)
    REFERENCES kelas (id_kelas);

-- Reference: kelaspelajar_pelajar (table: kelas_pelajar)
ALTER TABLE kelas_pelajar ADD CONSTRAINT kelaspelajar_pelajar FOREIGN KEY kelaspelajar_pelajar (pelajar_id_pelajar)
    REFERENCES pelajar (id_pelajar);

-- Reference: kursus_sanggar (table: kursus)
ALTER TABLE kursus ADD CONSTRAINT kursus_sanggar FOREIGN KEY kursus_sanggar (sanggar_id_sanggar)
    REFERENCES sanggar (id_sanggar);

-- Reference: kursus_tag_kursus (table: kursus_tag)
ALTER TABLE kursus_tag ADD CONSTRAINT kursus_tag_kursus FOREIGN KEY kursus_tag_kursus (kursus_id_kursus)
    REFERENCES kursus (id_kursus);

-- Reference: kursus_tag_tag (table: kursus_tag)
ALTER TABLE kursus_tag ADD CONSTRAINT kursus_tag_tag FOREIGN KEY kursus_tag_tag (tag_id_tag)
    REFERENCES tag (id_tag);

-- Reference: pelajar_users (table: pelajar)
ALTER TABLE pelajar ADD CONSTRAINT pelajar_users FOREIGN KEY pelajar_users (users_id_user)
    REFERENCES users (id_user);

-- Reference: sanggar_users (table: sanggar)
ALTER TABLE sanggar ADD CONSTRAINT sanggar_users FOREIGN KEY sanggar_users (users_id_user)
    REFERENCES users (id_user);

-- End of file.

