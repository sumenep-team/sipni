<?php

namespace App\Infrastructure\Query\MySql;

use App\Application\Query\Kursus\KursusDto;
use App\Application\Query\Kursus\KursusQueryInterface;
use Illuminate\Support\Facades\DB;

class KursusQuery implements KursusQueryInterface
{
    public function execute(string $Id): ?KursusDto
    {
        $result = DB::table('kursus')->where('id_kursus', $Id)->first();

        if(!$result) {
            return null;
        }
        return new KursusDto(
            $result->id,
            $result->foto_kursus,
            $result->nama_kursus,
            $result->deskripsi,
            $result->tempat,
            $result->tag,
            $result->is_dewasa
        );
    }
}
