<?php

namespace App\Infrastructure\Query\MySql;
use App\Application\Query\Kursus\DaftarKursusQueryInterface;
use App\Application\Query\Kursus\KursusDto;
use Illuminate\Support\Facades\DB;

class DaftarKursusQuery implements DaftarKursusQueryInterface
{
    public function execute(string $id_sanggar): ?array
    {
        $rows = DB::table('kursus')->where("sanggar_id_sanggar", $id_sanggar);
        if(!$rows)
        {
            return null;
        }

        $daftar_kursus = array();

        foreach($rows as $row)
        {
            array_push(
                $daftar_kursus,
                new KursusDto(
                    $row->id_kursus,
                    $row->foto_kursus,
                    $row->nama_kursus,
                    $row->deskripsi,
                    $row->tempat,
                    $row->tag,
                    $row->is_dewasa
                )
            );
        }

        return $daftar_kursus;
    }
}