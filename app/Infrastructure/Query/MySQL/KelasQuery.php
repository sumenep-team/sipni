<?php

namespace App\Infrastructure\Query\MySql;

use App\Application\Query\Kelas\KelasDto;
use App\Application\Query\Kelas\KelasQueryInterface;
use Illuminate\Support\Facades\DB;

class KelasQuery implements KelasQueryInterface
{
    public function execute(string $kelasId) : ?KelasDto
    {
        $row = DB::table('kelas')
        ->join('kursus', 'kelas.kursus_id_kursus', '=', 'kursus.id_kursus')
        ->join('jadwal', 'kelas.id_kelas', '=', 'jadwal.kelas_id_kelas')
        ->where("id_kelas", $kelasId)->first();
        if(!$row)
        {
            return null;
        }

        return new KelasDto(
            $row->id_kelas,
            $row->nama,
            $row->kursus_id_kursus,
            $row->nama_kursus,
            $row->instruktur,
            $row->status,
            $row->kapasitas,
            $row->harga,
            $row->hari,
            $row->jam_mulai,
            $row->jam_selesai,
            $row->foto_kursus
        );
    }
}