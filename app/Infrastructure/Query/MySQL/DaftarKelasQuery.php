<?php


namespace App\Infrastructure\Query\MySQL;


use App\Application\Query\Kelas\DaftarKelasDto;
use App\Application\Query\Kelas\DaftarKelasQueryInterface;
use Illuminate\Support\Facades\DB;

class DaftarKelasQuery implements DaftarKelasQueryInterface
{
    public function execute(string $kursus_id) : ?array
    {
        $rows = DB::table('kelas')
            ->where("id_kursus", $kursus_id);
        if(!$rows)
        {
            return null;
        }
        $result = array();
        foreach ($rows as $row) {
            array_push($result, new DaftarKelasDto(
                $row->id_kelas,
                $row->nama,
                $row->instruktur,
                $row->status,
                $row->kapasitas,
            ));
        }
        return $result;
    }
}
