<?php


namespace App\Infrastructure\Repository\MySQL;

use App\Core\Models\Nominal\Nominal;
use App\Core\Models\Pelajar\DompetPelajar;
use App\Core\Models\Pelajar\DompetPelajarID;
use App\Core\Models\Pelajar\Pelajar;
use App\Core\Models\Pelajar\PelajarID;
use App\Core\Models\Pelajar\Wishlist;
use App\Core\Repository\PelajarRepositoryInterface;
use Illuminate\Support\Facades\DB;


class PelajarRepository implements PelajarRepositoryInterface
{
    public function byId(PelajarID $pelajar_id): ?Pelajar
    {
        $row_pelajar = DB::table('pelajar')->where(['id_pelajar', $pelajar_id->getId()])->first();
        if (!$row_pelajar) return null;
        $row_dompet = DB::table('dompet')->where(['users_id_user', $row_pelajar->users_id_user])->first();
        if (!$row_dompet) return null;
        $row_wishlist = DB::table('wishlist')->where(['pelajar_id_pelajar', $pelajar_id->getId()]);
        //        if (!$row_wishlist) return null;
        $wishlist = new Wishlist($pelajar_id->getId());
        foreach ($row_wishlist as $row) {
            $wishlist->addWishlist($row->kelas_id_kelas);
        }
        return new Pelajar(
            new PelajarID($row_pelajar->id_kelas),
            $row_pelajar->username,
            $row_pelajar->nama,
            $row_pelajar->deskripsi,
            $row_pelajar->kota,
            $row_pelajar->foto_profil,
            new DompetPelajar(new DompetPelajarID($row_dompet->id_dompet), new Nominal($row_dompet->saldo)),
            $wishlist
        );
    }

    public function update(Pelajar $pelajar): void
    {
        $payload = $this->createPayload($pelajar);
        DB::table('pelajar')
        ->where('id_pelajar', $pelajar->getPelajar_id()->getId())
        ->update($payload);
    }

    private function createPayload(Pelajar $pelajar)
    {
        return [
            "id_pelajar" => $pelajar->getPelajar_id()->getId(),
            "username" => $pelajar->getUsername(),
            "nama" => $pelajar->getNama(),
            "deskripsi" => $pelajar->getDeskripsi(),
            "kota" => $pelajar->getKota(),
            "foto_profil" => $pelajar->getFoto_profil_url()
        ];
    }
}
