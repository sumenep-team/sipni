<?php


namespace App\Infrastructure\Repository\MySQL;

use App\Core\Models\Kelas\Kelas;
use App\Core\Models\Kelas\KelasId;
use App\Core\Models\Kelas\StatusKelas;
use App\Core\Models\Kursus\KursusID;
use App\Core\Models\Nominal\Nominal;
use App\Core\Models\Pelajar\DompetPelajar;
use App\Core\Repository\KelasRepositoryInterface;
use Illuminate\Support\Facades\DB;

class KelasRepository implements KelasRepositoryInterface
{
    public function byId(KelasId $kelas_id): ?Kelas
    {
        $row = DB::table('kelas')->where(['id_kelas', $kelas_id->getId()])->first();
        if (!$row) return null;
        return new Kelas(
            new KelasID($row->id_kelas),
            new KursusID($row->kursus_id_kursus),
            $row->nama_kelas,
            $row->instruktur,
            new StatusKelas($row->status),
            $row->kapasitas,
            $row->jumlah_peserta,
            new Nominal($row->harga)
        );
    }

    public function save(Kelas $kelas): void
    {
        $payload = $this->createPayload($kelas);
        DB::table('kelas')->insert($payload);
    }

    public function delete(Kelas $kelas): void
    {
        DB::table('kelas')->delete($kelas->getKelas_id()->getId());
    }

    private function createPayload(Kelas $kelas)
    {
        return [
            "id_kelas" => $kelas->getKelas_id()->getId(),
            "nama_kelas" => $kelas->getNama_kelas(),
            "instruktur" => $kelas->getInstruktur(),
            "status" => $kelas->getStatus_kelas()->getStatus(),
            "kapasitas" => $kelas->getKapasitas(),
            "jumlah_peserta" => $kelas->getJumlah_peserta(),
            "harga" => $kelas->getHarga()
        ];
    }
}
