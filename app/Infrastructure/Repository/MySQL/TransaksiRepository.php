<?php
namespace App\Infrastructure\Repository\MySQL;
use App\Core\Models\Nominal\Nominal;
use App\Core\Models\Pelajar\PelajarID;
use App\Core\Models\Transaksi\Transaksi;
use App\Core\Models\Transaksi\TransaksiID;
use App\Core\Repository\TransaksiRepositoryInterface;
use Illuminate\Support\Facades\DB;


class TransaksiRepository implements TransaksiRepositoryInterface
{
    public function byId(TransaksiID $transaksi_id): ?Transaksi
    {
        $row = DB::table('transaksi')->where('id_transaksi', $transaksi_id->getId())->first();
        if(!$row)
        {
            return null;
        }
        return new Transaksi(
            $transaksi_id,
            new PelajarID($row->pelajar_id_pelajar),
            new Nominal($row->nominal),
            $row->foto_bukti_transaksi
        );
    }

    public function save(Transaksi $transaksi, string $pelajar_id): void
    {
        $payload = [
            'id_transaksi' => $transaksi->getTransaksiID()->getId(),
            'nominal' => $transaksi->getTransaksiNominal()->getNominalNomor(),
            'foto_bukti_transaksi' => $transaksi->getFoto(),
            'status' => $transaksi->getStatus()->getStatus(),
            'pelajar_id_pelajar' => $pelajar_id
        ];
        DB::table('transaksi')->insert($payload);
    }

    public function update(Transaksi $transaksi): void
    {
        $payload = [
            'nominal' => $transaksi->getTransaksiNominal()->getNominalNomor(),
            'foto_bukti_transaksi' => $transaksi->getFoto(),
            'status' => $transaksi->getStatus()->getStatus()
        ];
        DB::table('transaksi')->where('id_transaksi', $transaksi->getTransaksiID()->getId())->update($payload);
    }
}
