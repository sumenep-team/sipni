<?php


namespace App\Infrastructure\Repository\MySQL;

use App\Core\Models\Nominal\Nominal;
use App\Core\Models\Sanggar\DompetSanggar;
use App\Core\Models\Sanggar\DompetSanggarID;
use App\Core\Models\Sanggar\Sanggar;
use App\Core\Models\Sanggar\SanggarID;
use App\Core\Repository\SanggarRepositoryInterface;
use Illuminate\Support\Facades\DB;

class SanggarRepository implements SanggarRepositoryInterface
{
    public function byId(SanggarID $sanggar_id): ?Sanggar
    {
        $row = DB::table('sanggar')->where(['id_sanggar', $sanggar_id->getId()])->first();
        if (!$row) return null;
        $row_dompet = DB::table('dompet')->where(['users_id_user', $row->users_id_user])->first();
        if(!$row_dompet) return null;
        return new Sanggar(
            new SanggarID($row->id_sanggar),
            new DompetSanggar(
                new DompetSanggarID($row_dompet->id_dompet),
                new Nominal($row_dompet->saldo)
            ),
            $row->username,
            $row->nama,
            $row->deskripsi,
            $row->kota,
            $row->no_rek,
            $row->foto_profil,
        );
    }

    public function update(Sanggar $sanggar): void
    {
        $payload = $this->createPayload($sanggar);
        DB::table('sanggar')
        ->where('id_sanggar', $sanggar->getSanggarID()->getId())
        ->update($payload);
    }

    private function createPayload(Sanggar $sanggar)
    {
        return [
            "id_sanggar" => $sanggar->getSanggarID()->getId(),
            "username" => $sanggar->getUsername(),
            "nama" => $sanggar->getNama(),
            "deskripsi" => $sanggar->getDeskripsi(),
            "kota" => $sanggar->getKota(),
            "no_rek" => $sanggar->getNoRsekening(),
            "foto_profil" => $sanggar->getFoto_profil_url()
        ];
    }
}
