<?php
namespace App\Infrastructure\Repository\MySQL;
use App\Core\Models\Kursus\Kursus;
use App\Core\Models\Kursus\KursusID;
use App\Core\Repository\KursusRepositoryInterface;
use Illuminate\Support\Facades\DB;


class KursusRepository implements KursusRepositoryInterface
{
    public function byId(KursusID $kursus_id): ?Kursus
    {
        $row = DB::table('kursus')->where('id_kursus', $kursus_id->getId())->first();
        if(!$row)
        {
            return null;
        }
        return new Kursus(
            $kursus_id,
            $row->nama_kursus,
            $row->deskripsi ? $row->deskripsi : null,
            $row->tempat ? $row->tempat : null,
            $row->tag ? $row->tag : null,
            $row->is_dewasa,
            $row->foto_kursus ? $row->foto_kursus : null
        );
    }

    public function save(Kursus $kursus, string $sanggar_id): void
    {
        $payload = [
            'id_kursus' => $kursus->getKursusId()->getId(),
            'nama_kursus' => $kursus->getNamaKursus(),
            'deskripsi' => $kursus->getDeskripsi(),
            'tempat' => $kursus->getLokasi(),
            'tag' => $kursus->getTag(),
            'is_dewasa' => $kursus->getIs_dewasa(),
            'foto_kursus' => $kursus->getUrl_foto(),
            'sanggar_id_sanggar' => $sanggar_id
        ];
        DB::table('kursus')->insert($payload);
    }
}
