<?php
namespace App\Infrastructure\Repository\MySQL;

use App\Core\Models\Kelas\KelasID;
use App\Core\Models\Kelas\PesertaKelas;
use App\Core\Models\Kelas\PesertaKelasID;
use App\Core\Models\Pelajar\PelajarID;
use App\Core\Repository\PesertaKelasRepositoryInterface;
use Illuminate\Support\Facades\DB;


class PesertaKelasRepository implements PesertaKelasRepositoryInterface
{
    public function byId(PesertaKelasID $peserta_kelas): ?PesertaKelas
    {
        $row = DB::table('kelas_pelajar')->where('id_kelaspelajar', $peserta_kelas->getId())->first();
        if(!$row)
        {
            return null;
        }
        return new PesertaKelas(
            $peserta_kelas,
            new KelasID($row->kelas_id_kelas),
            new PelajarID($row->pelajar_id_pelajar)
        );
    }
    public function byPelajarIDandKelasID(PelajarID $pelajar_id, KelasID $kelas_id): ?PesertaKelas
    {
        $row = DB::table('kelas_pelajar')->where([
            ['pelajar_id_pelajar', $pelajar_id->getId()],
            ['kelas_id_kelas', $kelas_id->getId()]
        ])->first();
        if(!$row)
        {
            return null;
        }
        return new PesertaKelas(
            new PesertaKelasID($row->id_kelaspelajar),
            $kelas_id,
            $pelajar_id
        );
    }

    public function delete(PesertaKelasID $id): void
    {
        DB::table('kelas_pelajar')->where('id_kelaspelajar', $id->getId())->delete();
    }
}
