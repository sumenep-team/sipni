<?php
namespace App\Infrastructure\Repository\MySQL;
use App\Core\Models\Admin\Admin;
use App\Core\Models\Admin\AdminID;
use App\Core\Repository\AdminRepositoryInterface;
use Illuminate\Support\Facades\DB;



class AdminRepository implements AdminRepositoryInterface
{
    public function byId(AdminID $admin_id): ?Admin
    {
        $row = DB::table('admin')->where('id_admin', $admin_id->getId())->first();
        if(!$row)
        {
            return null;
        }
        return new Admin(
            $admin_id,
            $row->nama,
            $row->foto_profil ? $row->foto_profil : null
        );
    }
}
