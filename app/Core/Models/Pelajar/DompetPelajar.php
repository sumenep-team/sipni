<?php

namespace App\Core\Models\Pelajar;
use App\Core\Models\Nominal\Nominal;

class DompetPelajar
{
    private DompetPelajarID $dompet_pelajar_id;
    private Nominal $nominal;
    public function __construct(DompetPelajarID $dompet_pelajar_id, Nominal $nominal)
    {
        $this->dompet_pelajar_id = $dompet_pelajar_id;
        $this->nominal = $nominal;
    }

    public function tambahNominal(int $jumlah)
    {
        $jumlah_nominal_baru = $this->nominal->getNominalNomor() + $jumlah;
        $this->nominal = new Nominal($jumlah_nominal_baru);
    }

    public function kurangNominal(int $jumlah)
    {
        $jumlah_nominal_baru = $this->nominal->getNominalNomor() - $jumlah;
        $this->nominal = new Nominal($jumlah_nominal_baru);
    }
}