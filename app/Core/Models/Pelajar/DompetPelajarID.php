<?php

namespace App\Core\Models\Pelajar;

use Ramsey\Uuid\Uuid;
class DompetPelajarID
{
    private $id;
    
    public function __construct(string $id)
    {
        if(Uuid::isValid($id))
        {
            $this->id = $id;
        } else{
            throw new \InvalidArgumentException("Invalid Dompet Pelajar ID format");
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(DompetPelajarID $dompet_pelajar_id)
    {
        return $this->id === $dompet_pelajar_id->getId();
    }
}
