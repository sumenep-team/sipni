<?php

namespace App\Core\Models\Pelajar;

use Ramsey\Uuid\Uuid;
class PelajarID
{
    private $id;
    
    public function __construct(string $id)
    {
        // if(Uuid::isValid($id))
        // {
            $this->id = $id;
        // } else{
        //     throw new \InvalidArgumentException("Invalid Pelajar ID format");
        // }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(PelajarID $pelajar_id)
    {
        return $this->id === $pelajar_id->getId();
    }
}
