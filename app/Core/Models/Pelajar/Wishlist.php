<?php

namespace App\Core\Models\Pelajar;
use App\Core\Models\Kelas\KelasID;

class Wishlist
{
    const KELAS_MAKSIMUM = 50;

    private Pelajar $pelajar;
    private $kelas_list;
    
    public function __construct(Pelajar $pelajar)
    {
        $this->pelajar = $pelajar;
        $this->kelas_list = array();
    }

    public function isEmpty(): bool{
        return count($this->kelas_list) === 0;
    }

    public function getListSize(): int{
        return count($this->kelas_list);
    }

    public function addWishlist(KelasID $kelas_id): void{
        if($this->getListSize() >= self::KELAS_MAKSIMUM)
        {
            throw new \OutOfRangeException("Telah melebihi maksimum kapasitas wishlist");
        }
        for($i = 0; $i < $this->getListSize(); $i++)
        {
            if($kelas_id->equals($this->kelas_list[$i]))
            {
                throw new \InvalidArgumentException("Kelas sudah ada dalam wishlist");
            }
        }
        array_push($this->kelas_list, $kelas_id);
    }

    public function removeWishlist(KelasID $kelas_id)
    {
        if($this->isEmpty())
        {
            throw new \InvalidArgumentException("Wishlist kosong");
        }

        for($i = 0; $i < $this->getListSize(); $i++)
        {
            if($kelas_id->equals($this->kelas_list[$i]))
            {
                array_splice($this->kelas_list, $i, 1);
                return;
            }
        }

        throw new \InvalidArgumentException("Kelas tidak termasuk dalam Wishlist");
    }


	/**
	 * @return Pelajar
	 */
	public function getPelajar(): Pelajar {
		return $this->pelajar;
	}
	
	/**
	 * @return mixed
	 */
	public function getKelasList() {
		return $this->kelas_list;
	}
}