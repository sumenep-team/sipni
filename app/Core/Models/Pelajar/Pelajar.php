<?php
namespace App\Core\Models\Pelajar;

class Pelajar
{
    private PelajarID $pelajar_id;
	private string $username;
	private string $nama;
	private string $deskripsi;
	private string $kota;
	private string $foto_profil_url;
	private DompetPelajar $dompet_pelajar;
	private Wishlist $wishlist;

    public function __construct(
        PelajarID $pelajar_id, 
        string $username,
        string $nama,
		?string $deskripsi,
		?string $kota,
		?string $foto_profil,
		DompetPelajar $dompet_pelajar,
		Wishlist $wishlist
        )
    {   
		$this->pelajar_id = $pelajar_id;
		$this->username = $username;
		$this->nama = $nama;
		$this->deskripsi = $deskripsi ? $deskripsi : null;
		$this->kota = $kota ? $kota : null;
		$this->foto_profil_url = $foto_profil ? $foto_profil : null;
		$this->dompet_pelajar = $dompet_pelajar;
		$this->wishlist = $wishlist;
    }



	/**
	 * @return PelajarID
	 */
	public function getPelajar_id(): PelajarID {
		return $this->pelajar_id;
	}
	
	/**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}
	
	/**
	 * @return string
	 */
	public function getNama(): string {
		return $this->nama;
	}
	
	/**
	 * @return string
	 */
	public function getDeskripsi(): string {
		return $this->deskripsi;
	}
	
	/**
	 * @return string
	 */
	public function getKota(): string {
		return $this->kota;
	}
	
	/**
	 * @return string
	 */
	public function getFoto_profil_url(): string {
		return $this->foto_profil_url;
	}

	/**
	 * @return DompetPelajar
	 */
	public function getDompet_pelajar(): DompetPelajar {
		return $this->dompet_pelajar;
	}
	
	/**
	 * @return Wishlist
	 */
	public function getWishlist(): Wishlist {
		return $this->wishlist;
	}
}
