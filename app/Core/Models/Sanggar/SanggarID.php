<?php

namespace App\Core\Models\Sanggar;

use Ramsey\Uuid\Uuid;
class SanggarID
{
    private $id;
    
    public function __construct(string $id)
    {
        if(Uuid::isValid($id))
        {
            $this->id = $id;
        } else{
            throw new \InvalidArgumentException("Invalid Pelajar ID format");
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(SanggarID $sanggar_id)
    {
        return $this->id === $sanggar_id->getId();
    }
}
