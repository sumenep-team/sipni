<?php

namespace App\Core\Models\Sanggar;
use App\Core\Models\Nominal\Nominal;

class DompetSanggar
{
    private DompetSanggarID $dompet_sanggar_id;
    private Nominal $nominal;
    public function __construct(DompetSanggarID $dompet_sanggar_id, Nominal $nominal)
    {
        $this->dompet_sanggar_id = $dompet_sanggar_id;
        $this->nominal = $nominal;
    }

    public function tambahNominal(int $jumlah)
    {
        $jumlah_nominal_baru = $this->nominal->getNominalNomor() + $jumlah;
        $this->nominal = new Nominal($jumlah_nominal_baru);
    }

    public function kurangNominal(int $jumlah)
    {
        $jumlah_nominal_baru = $this->nominal->getNominalNomor() - $jumlah;
        $this->nominal = new Nominal($jumlah_nominal_baru);
    }
}