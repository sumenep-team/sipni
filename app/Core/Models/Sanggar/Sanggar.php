<?php
namespace App\Core\Models\Sanggar;

class Sanggar
{
    private SanggarID $sanggar_id;
	private DompetSanggar $dompet_sanggar;
	private string $username;
	private string $nama;
	private string $deskripsi;
	private string $kota;
	private string $no_rekening;
	private string $foto_profil_url;

    public function __construct(
        SanggarID $sanggar_id, 	
		DompetSanggar $dompet_sanggar,
		string $username,
		string $nama,
		?string $deskripsi,
		?string $kota,
		string $no_rekening,
		?string $foto_profil_url,
        )
    {   
		$this->sanggar_id = $sanggar_id;
		$this->dompet_sanggar = $dompet_sanggar;
		$this->username = $username;
		$this->nama = $nama;
		$this->deskripsi = $deskripsi ? $deskripsi : null;
		$this->kota = $kota ? $kota : null;
		$this->no_rekening = $no_rekening;
		$this->foto_profil_url = $foto_profil_url ? $foto_profil_url : null;
    }

	/**
	 * @return SanggarID
	 */
	public function getSanggarID(): SanggarID {
		return $this->sanggar_id;
	}
	
	/**
	 * @return string
	 */
	public function getUsername(): string {
		return $this->username;
	}
	
	/**
	 * @return string
	 */
	public function getNama(): string {
		return $this->nama;
	}
	
	/**
	 * @return string
	 */
	public function getDeskripsi(): string {
		return $this->deskripsi;
	}
	
	/**
	 * @return string
	 */
	public function getKota(): string {
		return $this->kota;
	}
	
	/**
	 * @return string
	 */
	public function getNoRsekening(): string {
		return $this->no_rekening;
	}
	
	/**
	 * @return string
	 */
	public function getFoto_profil_url(): string {
		return $this->foto_profil_url;
	}

	/**
	 * @return DompetSanggar
	 */
	public function getDompet_sanggar(): DompetSanggar {
		return $this->dompet_sanggar;
	}
}
