<?php

namespace App\Core\Models\Sanggar;

use Ramsey\Uuid\Uuid;
class DompetSanggarID
{
    private $id;
    
    public function __construct(string $id)
    {
        if(Uuid::isValid($id))
        {
            $this->id = $id;
        } else{
            throw new \InvalidArgumentException("Invalid Dompet Sanggar ID format");
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(DompetSanggarID $dompet_sanggar_id)
    {
        return $this->id === $dompet_sanggar_id->getId();
    }
}
