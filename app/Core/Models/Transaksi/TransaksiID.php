<?php

namespace App\Core\Models\Transaksi;

use Ramsey\Uuid\Uuid;
class TransaksiID
{
    private $id;
    
    public function __construct(string $id)
    {
        if(Uuid::isValid($id))
        {
            $this->id = $id;
        } else{
            throw new \InvalidArgumentException("Invalid Pelajar ID format");
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(TransaksiID $transaksi_id)
    {
        return $this->id === $transaksi_id->getId();
    }
}
