<?php

namespace App\Core\Models\Transaksi;

use App\Core\Models\Pelajar\PelajarID;
use App\Core\Models\Nominal\Nominal;

class Transaksi
{
    private StatusTransaksi $status_transaksi;
    public function __construct(
        private TransaksiID $transaksi_id,
        private PelajarID $pelajar_id,
        private Nominal $nominal,
        public string $foto_bukti_transaksi
    ) {
        $this->status_transaksi = StatusTransaksi::belumDiverifikasi();
    }

    public function getTransaksiID() {
        return $this->transaksi_id;
    }

    public function getTransaksiNominal() {
        return $this->nominal;
    }

    public function getFoto() {
        return $this->foto_bukti_transaksi;
    }

    public function getStatus() {
        return $this->status_transaksi;
    }

    public function accept()
    {
        $this->status_transaksi = StatusTransaksi::disetujui();
    }
    public function reject()
    {
        $this->status_transaksi = StatusTransaksi::ditolak();
    }


}
