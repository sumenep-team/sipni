<?

namespace App\Core\Models\Transaksi;

class StatusTransaksi
{
    const BELUM_DIVERIFIKASI = '1';
    const DISETUJUI = '2';
    const DITOLAK = '3';

    private string $status;
    public function __construct(string $status)
    {
        if($status != self::BELUM_DIVERIFIKASI && $status != self::DISETUJUI && $status != self::DITOLAK)
        {
            throw new \InvalidArgumentException("Transaksi Status Code Invalid");
        }

        $this->status = $status;
    }

    public function equals(StatusTransaksi $status_transaksi): bool
    {
        return $this->status === $status_transaksi->status;
    }

    public function isSudahDiverifikasi(): bool
    {
        if ($this->status === self::BELUM_DIVERIFIKASI) {
            return false;
        }
        return true;
    }

    public function isDisetuji(): bool
    {
        if ($this->status === self::DISETUJUI) {
            return true;
        }
        return false;
    }

    public function isDitolak(): bool
    {
        if($this->status === self::DITOLAK)
        {
            return true;
        }
        return false;
    }
	/**
	 * @return string
	 */
	public function getStatus(): string {
		return $this->status;
	}

    public static function belumDiverifikasi()
    {
        return new StatusTransaksi(self::BELUM_DIVERIFIKASI);
    }
    public static function disetujui()
    {
        return new StatusTransaksi(self::DISETUJUI);
    }
    public static function ditolak()
    {
        return new StatusTransaksi(self::DITOLAK);
    }
}