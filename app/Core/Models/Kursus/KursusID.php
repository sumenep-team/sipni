<?php

namespace App\Core\Models\Kursus;

use Ramsey\Uuid\Uuid;
class KursusID
{
    private $id;
    
    public function __construct(string $id)
    {
        // if(Uuid::isValid($id))
        // {
            $this->id = $id;
        // } else{
        //     throw new \InvalidArgumentException("Invalid Kursus ID format");
        // }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(KursusID $kursus_id)
    {
        return $this->id === $kursus_id->getId();
    }
}
