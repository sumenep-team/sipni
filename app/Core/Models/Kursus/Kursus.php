<?php
namespace App\Core\Models\Kursus;

class Kursus
{
    private KursusID $kursus_id;
    private string $nama_kursus;
    private ?string $deskripsi;
    private ?string $lokasi;
    private ?string $tag;
    private bool $is_dewasa;
    private ?string $url_foto;
    public function __construct(
        KursusID $kursus_id,
        string $nama_kursus,
        ?string $deskripsi,
        ?string $lokasi,
        ?string $tag,
        bool $is_dewasa,
        ?string $url_foto
        )
    {
        $this->kursus_id = $kursus_id;
        $this->nama_kursus = $nama_kursus;
        $this->deskripsi = $deskripsi ? $deskripsi : null;
        $this->lokasi = $lokasi ? $lokasi : null;
        $this->tag = $tag ? $tag : null;
        $this->is_dewasa = $is_dewasa;
        $this->url_foto = $url_foto ? $url_foto : null;
    }

	/**
	 * @return KursusID
	 */
	public function getKursusId(): KursusID {
		return $this->kursus_id;
	}

	/**
	 * @return string
	 */
	public function getNamaKursus(): string {
		return $this->nama_kursus;
	}

	/**
	 * @return string
	 */
	public function getDeskripsi(): string {
		return $this->deskripsi;
	}

	/**
	 * @return string
	 */
	public function getLokasi(): string {
		return $this->lokasi;
	}

    /**
     * @return string
     */
    public function getTag(): string {
        return $this->tag;
    }

	/**
	 * @return bool
	 */
	public function getIs_dewasa(): bool {
		return $this->is_dewasa;
	}

	/**
	 * @return string
	 */
	public function getUrl_foto(): string {
		return $this->url_foto;
	}
}
