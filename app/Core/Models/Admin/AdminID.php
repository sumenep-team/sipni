<?php

namespace App\Core\Models\Admin;

use Ramsey\Uuid\Uuid;
class AdminID
{
    private $id;
    
    public function __construct(string $id)
    {
        if(Uuid::isValid($id))
        {
            $this->id = $id;
        } else{
            throw new \InvalidArgumentException("Invalid Admin ID format");
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(AdminID $admin_id)
    {
        return $this->id === $admin_id->getId();
    }
}
