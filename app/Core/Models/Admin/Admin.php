<?php

namespace App\Core\Models\Admin;

class Admin
{
    private AdminID $admin_id;
    private string $nama;
    private string $foto_url;

    public function __construct(AdminID $admin_id, string $nama, ?string $foto_url)
    {
        $this->admin_id = $admin_id;
        $this->nama = $nama;
        $this->foto_url = $foto_url ? $foto_url : null;
    }

	/**
	 * @return AdminID
	 */
	public function getAdminId(): AdminID {
		return $this->admin_id;
	}
	
	/**
	 * @return string
	 */
	public function getNama(): string {
		return $this->nama;
	}
	
	/**
	 * @return string
	 */
	public function getFotoUrl(): string {
		return $this->foto_url;
	}
}
