<?php

namespace App\Core\Models\Nominal;

class Nominal
{
    private string $nominal;
    private int $nominal_nomor;
    
    public function __construct(int $nominal_nomor)
    {
        $this->nominal_nomor = $nominal_nomor;
        $this->nominal = strval($nominal_nomor);
    }

	/**
	 * @return string
	 */
	public function getNominal(): string {
		return $this->nominal;
	}
	
	/**
	 * @return int
	 */
	public function getNominalNomor(): int {
		return $this->nominal_nomor;
	}

    public function isEqual(Nominal $nominal): bool {
        return $this->nominal_nomor === $nominal->getNominalNomor();
    }

    public function isGreaterThan(Nominal $nominal): bool {
        return $this->nominal_nomor > $nominal->getNominalNomor();
    }

    public function isLessThan(Nominal $nominal): bool {
        return $this->nominal_nomor < $nominal->getNominalNomor();
    }
}