<?
namespace App\Core\Models\Kelas;
use InvalidArgumentException;

class Waktu
{
    private string $waktu;
    private string $jam;
    private string $menit;

    public function __construct(string $waktu_string)
    {
        if(!preg_match('/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/', $waktu_string))
        {
            throw new InvalidArgumentException("Waktu format invalid");
        }

        $waktu_array = explode(":", $waktu_string);

        $this->jam = $waktu_array[0];
        $this->menit = $waktu_array[1];
        $this->waktu = $this->jam.":".$this->menit;
    }

    public function equals(Waktu $waktu): bool
    {
        return strcmp($waktu->getWaktu(), $this->getWaktu()) == 0;
    }
    

	/**
	 * @return string
	 */
	public function getWaktu(): string {
		return $this->waktu;
	}
	
	/**
	 * @return string
	 */
	public function getJam(): string {
		return $this->jam;
	}
	
	/**
	 * @return string
	 */
	public function getMenit(): string {
		return $this->menit;
	}
}