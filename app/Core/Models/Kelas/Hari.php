<?
namespace App\Core\Models\Kelas;

class Hari
{
    const SENIN = '1';
    const SELASA = '2';
    const RABU = '3';
    const KAMIS = '4';
    const JUMAT = '5';
    const SABTU = '6';
    const MINGGU = '7';
    private string $hari;

    public function __construct(int $hari_kode)
    {
        if($hari_kode < 1 || $hari_kode > 7)
        {
            throw new \InvalidArgumentException("Hari status code invalid");
        }
        if($hari_kode == 1)
        {
            $this->hari = self::SENIN;
        }
        else if ($hari_kode == 2)
        {
            $this->hari = self::SELASA;
        }
        else if ($hari_kode == 3)
        {
            $this->hari = self::RABU;
        }
        else if ($hari_kode == 4)
        {
            $this->hari = self::KAMIS;
        }
        else if ($hari_kode == 5)
        {
            $this->hari = self::JUMAT;
        }
        else if ($hari_kode == 6)
        {
            $this->hari = self::SABTU;
        }
        else
        {
            $this->hari = self::MINGGU;
        }
    }

    public function equals(Hari $hari): bool
    {
        return $this->hari === $hari->hari;
    }
	/**
	 * @return string
	 */
	public function getHari(): string {
		return $this->hari;
	}
}