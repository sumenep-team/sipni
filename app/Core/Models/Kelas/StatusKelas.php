<?

namespace App\Core\Models\Kelas;

class StatusKelas
{
    const BELUM_DIMULAI = '1';
    const SEDANG_BERLANGSUNG = '2';
    const SELESAI = '3';
    private string $status;
    public function __construct(int $status)
    {
        if($status < 1 || $status > 3)
        {
            throw new \InvalidArgumentException("Kelas Status Code Invalid");
        }

        if($status == 1)
        {
            $this->status = self::BELUM_DIMULAI;
        }
        else if($status == 2)
        {
            $this->status = self::SEDANG_BERLANGSUNG;
        }
        else
        {
            $this->status = self::SELESAI;
        }
    }

    public function equals(StatusKelas $status_kelas): bool
    {
        return $this->status === $status_kelas->status;
    }

    public function isSedangBerlangsung(): bool
    {
        if ($this->status === self::SEDANG_BERLANGSUNG) {
            return true;
        }
        return false;
    }

    public function isSelesai(): bool
    {
        if ($this->status === self::SELESAI) {
            return true;
        }
        return false;
    }
    
	/**
	 * @return string
	 */
	public function getStatus(): string {
		return $this->status;
	}
}