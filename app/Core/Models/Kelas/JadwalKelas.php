<?

namespace App\Core\Models\Kelas;
use App\Core\Models\Kursus\KursusID;
class JadwalKelas
{
    public function __construct(
        private Hari $hari,
        private Waktu $waktu_mulai,
        private Waktu $waktu_selesai,
        private KursusID $kursus_id
    )
    {
    }

	/**
	 * @return Hari
	 */
	public function getHari(): Hari {
		return $this->hari;
	}
	
	/**
	 * @return Waktu
	 */
	public function getWaktuMulai(): Waktu {
		return $this->waktu_mulai;
	}
	
	/**
	 * @return Waktu
	 */
	public function getWaktuSelesai(): Waktu {
		return $this->waktu_selesai;
	}

    public function getKursusID(): KursusID {
        return $this->kursus_id;
    }
}