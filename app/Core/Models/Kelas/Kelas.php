<?

namespace App\Core\Models\Kelas;
use App\Core\Models\Kursus\KursusID;
use App\Core\Models\Nominal\Nominal;

class Kelas
{
    public function __construct(
        private KelasID $kelas_id,
        private KursusID $kursus_id,
        private string $nama_kelas,
        private string $instruktur,
        private StatusKelas $status_kelas,
        private int $kapasitas,
        private int $jumlah_peserta,
        private Nominal $harga
        )
    {
    }

	/**
	 * @return KelasID
	 */
	public function getKelas_id(): KelasID {
		return $this->kelas_id;
	}
	
	/**
	 * @return KursusID
	 */
	public function getKursus_id(): KursusID {
		return $this->kursus_id;
	}
	
	/**
	 * @return string
	 */
	public function getNama_kelas(): string {
		return $this->nama_kelas;
	}
	
	/**
	 * @return string
	 */
	public function getInstruktur(): string {
		return $this->instruktur;
	}
	
	/**
	 * @return StatusKelas
	 */
	public function getStatus_kelas(): StatusKelas {
		return $this->status_kelas;
	}
	
	/**
	 * @return int
	 */
	public function getKapasitas(): int {
		return $this->kapasitas;
	}
	
	/**
	 * @return int
	 */
	public function getJumlah_peserta(): int {
		return $this->jumlah_peserta;
	}
	
	/**
	 * @return Nominal
	 */
	public function getHarga(): Nominal {
		return $this->harga;
	}
}
