<?

namespace App\Core\Models\Kelas;

use Ramsey\Uuid\Uuid;


class KelasID
{
    private $id;
    
    public function __construct(string $id)
    {
        // if(Uuid::isValid($id))
        // {
            $this->id = $id;
        // } else{
        //     throw new \InvalidArgumentException("Invalid Kelas ID format");
        // }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(KelasID $kelas_id)
    {
        return $this->id === $kelas_id->getId();
    }
}