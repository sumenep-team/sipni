<?

namespace App\Core\Models\Kelas;

use Ramsey\Uuid\Uuid;


class PesertaKelasID
{
    private $id;
    
    public function __construct(string $id)
    {
        if(Uuid::isValid($id))
        {
            $this->id = $id;
        } else{
            throw new \InvalidArgumentException("Invalid Peserta Kelas ID format");
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function equals(PesertaKelasID $peserta_kelas_id)
    {
        return $this->id === $peserta_kelas_id->getId();
    }
}