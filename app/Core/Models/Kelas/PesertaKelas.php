<?php

namespace App\Core\Models\Kelas;

use App\Core\Models\Pelajar\PelajarID;

class PesertaKelas
{
    private PesertaKelasID $id;
    private KelasID $kelas_id;
    private PelajarID $pelajar_id;

	/**
	 * @return PesertaKelasID
	 */
	public function getId(): PesertaKelasID {
		return $this->id;
	}

    public function __construct(PesertaKelasID $id, KelasID $kelas_id, PelajarID $pelajar_id) {
        $this->id = $id;
        $this->kelas_id = $kelas_id;
        $this->pelajar_id = $pelajar_id;
    }
}