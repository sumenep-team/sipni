<?php
namespace App\Core\Repository;
use App\Core\Models\Admin\Admin;
use App\Core\Models\Admin\AdminID;
interface AdminRepositoryInterface
{
    public function byId(AdminID $admin_id): ?Admin;
}