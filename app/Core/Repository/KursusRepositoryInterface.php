<?php
namespace App\Core\Repository;
use App\Core\Models\Kursus\Kursus;
use App\Core\Models\Kursus\KursusID;
interface KursusRepositoryInterface
{
    public function byId(KursusID $kursus_id): ?Kursus;
    public function save(Kursus $kursus, string $sanggar_id): void;
}
