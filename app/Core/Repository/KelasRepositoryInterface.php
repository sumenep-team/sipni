<?php

namespace App\Core\Repository;

use App\Core\Models\Kelas\Kelas;
use App\Core\Models\Kelas\KelasID;

interface KelasRepositoryInterface
{
    public function byId(KelasID $id): ?Kelas;
    public function save(Kelas $kelas): void;    
    public function delete(Kelas $kelas): void;
}
