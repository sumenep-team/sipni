<?php

namespace App\Core\Repository;

use App\Core\Models\Kelas\KelasID;
use App\Core\Models\Kelas\PesertaKelas;
use App\Core\Models\Kelas\PesertaKelasID;
use App\Core\Models\Pelajar\PelajarID;

interface PesertaKelasRepositoryInterface
{
    public function byId(PesertaKelasID $id): ?PesertaKelas;
    public function byPelajarIDandKelasID(PelajarID $pelajar_id, KelasID $kelas_id): ?PesertaKelas;
    public function delete(PesertaKelasID $id): void;
}
