<?php
namespace App\Core\Repository;
use App\Core\Models\Transaksi\Transaksi;
use App\Core\Models\Transaksi\TransaksiID;

interface TransaksiRepositoryInterface
{
    public function byId(TransaksiID $transaksi_id): ?Transaksi;
    public function save(Transaksi $transaksi, string $pelajar_id): void;
    public function update(Transaksi $transaksi): void;
}
