<?php

namespace App\Core\Repository;

use App\Core\Models\Pelajar\Pelajar;
use App\Core\Models\Pelajar\PelajarID;

interface PelajarRepositoryInterface
{
    public function byId(PelajarID $id): ?Pelajar;
    public function update(Pelajar $pelajar): void;
}
