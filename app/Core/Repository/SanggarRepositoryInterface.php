<?php

namespace App\Core\Repository;

use App\Core\Models\Sanggar\Sanggar;
use App\Core\Models\Sanggar\SanggarID;

interface SanggarRepositoryInterface
{
    public function byId(SanggarID $id): ?Sanggar;
    public function update(Sanggar $sanggar): void;
}
