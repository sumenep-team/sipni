<?php

namespace App\Http\Livewire\Pelajar\Telusuri;

use App\Application\Query\Kelas\KelasDto;
use App\Application\Query\Kursus\KursusDto;
use App\Application\Query\Sanggar\SanggarDto;
use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        // $kelasList = $this->daftarKelasQuery->execute();
        // $sanggarList = $this->daftarSanggarQuery->execute();

        $kelas = new KelasDto(
            6,
            'Tari Baris Dasar Juni 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );
        $kelas2 = new KelasDto(
            7,
            'Bocil Kematian Anthem 2023',
            7,
            'Bocil Kematian Anthem',
            'Arya Widia, S.kom.',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '123123123.jpg',
        );
        $kelasList = [$kelas, $kelas2];


        $profil_sanggar = new SanggarDto(
            6,
            '1687175321.jpg',
            'Sanggar Wiraga Ayu',
            '@wiraga_ayu',
            'Surabaya',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu cursus turpis, non aliquam elit. Suspendisse posuere euismod placerat. Sed ac nisl mauris. Curabitur vitae efficitur libero, non dignissim mauris. Nulla vehicula leo semper, dapibus tortor at, finibus sem. In ut ipsum metus. Duis sit amet nulla id est dictum rutrum sodales quis magna. Proin in tortor mollis, pellentesque metus sed, scelerisque arcu. Pellentesque vel mattis erat. Etiam vehicula enim quam. Aenean pretium ante orci, id lacinia purus condimentum sed.
        Sed vitae commodo ligula. Quisque vitae turpis turpis. Suspendisse at massa libero. Aliquam erat volutpat. Nullam velit augue, tincidunt quis urna imperdiet, finibus sodales tortor. Sed eu mattis neque. Donec convallis erat vitae erat pulvinar, ac consectetur tellus luctus. Mauris quis ex in enim interdum consequat quis vitae risus. Ut accumsan porta sem, dignissim lobortis lorem tempor cursus. In cursus faucibus nunc accumsan viverra. Ut finibus, lacus vitae auctor scelerisque, nibh nisl luctus sem, ut tempor mauris mauris a dolor. Curabitur lacus diam, tincidunt eget libero eget, porta faucibus est.',
        );
        $profil_sanggar2 = new SanggarDto(
            7,
            '112121212.jpg',
            'Sanggar Arya Widia',
            '@awia',
            'bli',
            'siap bersedia',
        );

        $sanggarList = [$profil_sanggar, $profil_sanggar2];

        return view('livewire.pelajar.telusuri.indexTelu', [ //TODO: change back to .index
            'kelasList' => $kelasList,
            'sanggarList' => $sanggarList,
        ]);
    }
}
