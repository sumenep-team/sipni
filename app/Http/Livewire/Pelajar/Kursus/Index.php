<?php

namespace App\Http\Livewire\Pelajar\Kursus;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.pelajar.kursus.index.php');
    }
}
