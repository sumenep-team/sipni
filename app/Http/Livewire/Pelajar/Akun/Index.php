<?php

namespace App\Http\Livewire\Pelajar\Akun;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.pelajar.akun.index');
    }
}
