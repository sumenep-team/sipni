<?php

namespace App\Http\Livewire\Pelajar\Wishlist;

use Livewire\Component;
use App\Application\Query\Kursus\KursusDto;

class Index extends Component
{
    public function render()
    {
        $kursusItem = new KursusDto(
            6,
            '1687175321.jpg',
            'Tari Baris Dasar',
            'Tari Baris Dasar deskripsi',
            'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
            'Tari, Tari Tradisional, Bali',
            true,
        );
        $kursusItem2 = new KursusDto(
            7,
            '1687175321.jpg',
            'Tari Baris Dasar New',
            'Tari Baris Dasar deskripsi',
            'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
            'Tari, Tari Tradisional, Bali',
            false
        );
        $kursusList = [$kursusItem, $kursusItem2];
        return view('livewire.pelajar.wishlist.index', ['kursusList' => $kursusList]);
    }
}
