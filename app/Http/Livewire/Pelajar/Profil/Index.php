<?php

namespace App\Http\Livewire\Pelajar\Profil;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.pelajar.profil.index');
    }
}
