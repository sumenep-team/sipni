<?php

namespace App\Http\Livewire\Sanggar\Kursus;

use App\Application\Query\Kursus\KursusDto;
use App\Application\Query\Kursus\DaftarKursusQueryInterface;
use App\Models\Category;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $kursus_id;
    public DaftarKursusQueryInterface $daftarKursusQuery;

    public function render()
    {
        //$kursusList = Kursus::orderBy('id', 'DESC')->paginate(5);
        //$kursusList = $this->daftarKursusQuery->execute();
        $kursusItem = new KursusDto(
            6,
            '1687175321.jpg',
            'Tari Baris Dasar',
            'Tari Baris Dasar deskripsi',
            'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
            'Tari, Tari Tradisional, Bali',
            true,
        );
        $kursusItem2 = new KursusDto(
            7,
            '1687175321.jpg',
            'Tari Baris Dasar New',
            'Tari Baris Dasar deskripsi',
            'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
            'Tari, Tari Tradisional, Bali',
            false,
        );
        $kursusList = [$kursusItem, $kursusItem2];
        return view('livewire.sanggar.kursus.index', ['kursusList' => $kursusList]);
    }
}
