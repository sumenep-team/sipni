<?php

namespace App\Http\Livewire\Sanggar\Akun;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.sanggar.akun.index');
    }
}
