<?php

namespace App\Http\Livewire\Sanggar\Kelas;

use App\Application\Command\HapusKelas\HapusKelasCommand;
use App\Application\Command\HapusKelas\HapusKelasRequest;
use App\Application\Query\Kelas\KelasDto;
use App\Application\Query\Kelas\KelasQueryInterface;
use App\Application\Query\Kursus\KursusQueryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Livewire\WithPagination;
use Throwable;

class Index extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $kelasId;
    public KelasQueryInterface $kelasQuery;
    private KursusQueryInterface $kursusQuery;

    public function render()
    {
        //$kelasList = $this->kelasQuery->execute();
        $kelas1 = new KelasDto(
            6,
            'Tari Baris Dasar Juni 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );

        $kelas2 = new KelasDto(
            7,
            'Tari Baris Dasar Juli 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );
        $kelasList = [$kelas1, $kelas2];
        return view('livewire.sanggar.kelas.index', ['kelasList' => $kelasList]);
    }
}
