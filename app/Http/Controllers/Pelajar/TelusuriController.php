<?php

namespace App\Http\Controllers\Pelajar;

use App\Application\Query\Kelas\KelasDto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TelusuriController extends Controller
{
    public function index(){
        return view('pelajar.telusuri.index');
    }
    public function kelasOfSanggar($sanggar_id){
        // $kelasList = $this->daftarKelasSanggarQuery->execute($sanggar_id);
        $kelas = new KelasDto(
            6,
            'Tari Baris Dasar Juni 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );
        $kelas2 = new KelasDto(
            7,
            'Bocil Kematian Anthem 2023',
            7,
            'Bocil Kematian Anthem',
            'Arya Widia, S.kom.',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '123123123.jpg',
        );
        $kelasList = [$kelas, $kelas2];
        return view('pelajar.kelas.index',[
            'kelasList' => $kelasList
        ]);
    }
    public function kelasDetails($kelas_id){
        $kelas = new KelasDto(
            6,
            'Tari Baris Dasar Juni 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );
        return view('pelajar.telusuri.detailkelas', [
            'kelas' => $kelas
        ]);
    }
    public function sanggarDetails($sanggar_id){
        return view('pelajar.telusuri.detailsanggar');
    }
}
