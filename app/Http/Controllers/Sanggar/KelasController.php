<?php

namespace App\Http\Controllers\Sanggar;

use App\Application\Command\BuatKelas\BuatKelasCommand;
use App\Application\Command\BuatKelas\BuatKelasRequest;
use App\Application\Query\Kelas\KelasDto;
use App\Application\Query\Kelas\KelasQueryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryFormRequest;
use App\Http\Requests\KelasFormRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Throwable;

class KelasController extends Controller
{
    //public KelasQueryInterface $kelasQuery; //TODO:KelasQueryInterface
    public function index(){
        return view('sanggar.kelas.index');
    }

    public function create(){
        return view('sanggar.kelas.create');
    }

    public function store(Request $request, BuatKelasCommand $command){
        $nama = $request->input('nama');
        $kursus_id = $request->input('kursus');
        $instruktur = $request->input('instruktur');
        $kapasitas = $request->input('kapasitas');
        $harga = $request->input('harga');
        $hari = $request->input('hari');
        $jamMulai = $request->input('jamMulai');
        $jamSelesai = $request->input('jamSelesai');

        $tambahRequest = new BuatKelasRequest(
            $nama,
            $kursus_id,
            $instruktur,
            $kapasitas,
            $harga,
            $hari,
            $jamMulai,
            $jamSelesai
        );

        try {
            $command->execute($tambahRequest);
        } catch (Throwable $e) {
            return back()->withErrors($e->getMessage())->withInput();
        }

        return response()->redirectTo(route('kelas'))
            ->with('message', 'Kelas berhasil ditambahkan!');
    }

    public function edit(Category $category){
        //$kelas = $this->kelasQuery->execute();
        $kelas = new KelasDto(
            7,
            'Tari Baris Dasar Juli 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );
        return view('sanggar.kelas.edit', compact('kelas'));
    }

    public function details($kelasId){
        //$kelas = $this->kelasQuery->execute();
        $kelas = new KelasDto(
            7,
            'Tari Baris Dasar Juli 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );
        $kelas2 = new KelasDto(
            7,
            'Tari Baris Dasar Juli 2023',
            7,
            'Tari Baris Dasar',
            'Sri Utami, S.Sn',
            1,
            10,
            100000,
            1,
            '12:00',
            '16:00',
            '1687175321.jpg',
        );
        $listPelajar = [$kelas, $kelas2];
        return view('sanggar.kelas.details', compact('kelas', 'listPelajar'));
    }
    public function update(CategoryFormRequest $request, $kelas)
    {
        $validatedData = $request->validated();

        $kelas = Category::findOrFail($kelas);

        //$kelas = new Category();

        $kelas->name = $validatedData['name'];
        $kelas->slug = Str::slug($validatedData['slug']);
        $kelas->description = $validatedData['description'];

        if($request->hasFile('image')){
            $path = 'uploads/kelas/'.$kelas->image;
            if(File::exists($path)){
                File::delete($path);
            }
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;

            $file->move('uploads/kelas/', $filename);
            $kelas->image = $filename;
        }

        $kelas->meta_title = $validatedData['meta_title'];
        $kelas->meta_keyword = $validatedData['meta_keyword'];
        $kelas->meta_description = $validatedData['meta_description'];

        $kelas->status = $request->status ? '1' : '0';
        $kelas->update();

        return redirect('sanggar/kelas')->with('message', 'Category updated successfully.');
    }
}
