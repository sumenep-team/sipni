<?php

namespace App\Http\Controllers\Sanggar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('sanggar.dashboard');
    }
}
