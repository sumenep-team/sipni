<?php

namespace App\Http\Controllers\Sanggar;

use App\Application\Command\BuatKursus\BuatKursusCommand;
use App\Application\Command\BuatKursus\BuatKursusRequest;
use App\Application\Query\Kursus\KursusDto;
use App\Application\Query\Kursus\KursusQueryInterface;
use App\Core\Models\Kursus\Kursus;
use App\Core\Repository\KursusRepositoryInterface;
use App\Core\Repository\SanggarRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryFormRequest;
use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class KursusController extends Controller
{
    public function __construct(
//        public KursusQueryInterface $kursusQuery,
        //public SanggarRepositoryInterface $sanggar_repository,
        //public KursusRepositoryInterface $kursus_repository
    )
    {
    }

    public function index(){
        return view('sanggar.kursus.index');
    }

    public function create(){
        return view('sanggar.kursus.create');
    }

    public function store(Request $request, BuatKursusCommand $command){
        $user = Auth::user();
        $sanggar_id = $user->id_user;
        if($request->hasFile('gambar')){
            $file = $request->file('gambar');
            $ext = $file->getClientOriginalExtension();
            $filename = time().'.'.$ext;

            $file->move('uploads/category/', $filename);
            $gambar = $filename;
        }

        $nama = $request->input('nama');
        $deskripsi = $request->input('deskripsi');
        $tempat = $request->input('tempat');
        $tags = $request->input('tags');
        $isDewasa = $request->input('isDewasa');

        $buatRequest = new BuatKursusRequest(
            $sanggar_id,
            $nama,
            $deskripsi,
            $tempat,
            $tags,
            $isDewasa,
            $gambar,
        );

        try {
            $command->execute($buatRequest);
        } catch (Exception $e) {
            return back()->withErrors($e->getMessage())->withInput();
        }

        return response()->redirectToRoute('kursusList')
            ->with('message', 'Category updated successfully.');

        //return redirect('sanggar/category')->with('message', 'Category added successfully.');
    }

//     public function edit($kursusId){
// //        $kursus = $this->kursusQuery->execute($kursusId); TODO:[fake dto]
//         $kursus = new KursusDto(
//             $kursusId,
//             '1687175321.jpg',
//             'Tari Baris Dasar',
//             'Tari Baris Dasar deskripsi',
//             'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
//             'Tari, Tari Tradisional, Bali',
//         );
//         return view('sanggar.kursus.edit', [
//             'kursus' => $kursus
//         ]);
    // }

    // public function update(Request $request, $category)
    // {
    //     $validatedData = $request->validated();

    //     $category = Category::findOrFail($category);

    //     //$category = new Category();

    //     $category->name = $validatedData['name'];
    //     $category->slug = Str::slug($validatedData['slug']);
    //     $category->description = $validatedData['description'];

    //     if($request->hasFile('image')){
    //         $path = 'uploads/category/'.$category->image;
    //         if(File::exists($path)){
    //             File::delete($path);
    //         }
    //         $file = $request->file('image');
    //         $ext = $file->getClientOriginalExtension();
    //         $filename = time().'.'.$ext;

    //         $file->move('uploads/category/', $filename);
    //         $category->image = $filename;
    //     }

    //     $pertemuanKe = $request->input('pertemuan-ke');
    //     $ruanganId = $request->input('ruangan');
    //     $tanggal = $request->input('tanggal');
    //     $jamMulai = $request->input('jam-mulai');
    //     $jamSelesai = $request->input('jam-selesai');
    //     $topik = $request->input('topik');
    //     $topikEn = $request->input('topik-en');
    //     $mode = $request->input('jenis-perkuliahan');

    //     $kelasId = 1;
    //         $pertemuanId = 1;

    //     $ubahRequest = new UbahPertemuanRequest(
    //         $kelasId,
    //         $pertemuanId,
    //         $pertemuanKe,
    //         $ruanganId,
    //         $tanggal,
    //         $jamMulai,
    //         $jamSelesai,
    //         $topik,
    //         $topikEn,
    //         $mode
    //     );

    //     $command = new UbahPertemuanCommand(
    //         $this->pertemuanRepository
    //     );

    //     try {
    //         $command->execute($ubahRequest);
    //     } catch (Exception $e) {
    //         return back()->withErrors($e->getMessage())->withInput();
    //     }

    //     return response()->redirectToRoute('kelas', ['id' => $kelasId])
    //         ->with('message', 'Category updated successfully.');

    //     //return redirect('sanggar/category')->with('message', 'Category updated successfully.');
    // }

}
