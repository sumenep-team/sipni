<?php


namespace App\Application\Command\TolakTransaksi;


use App\Application\Command\SetujuiTransaksi\SetujuiTransaksiRequest;
use App\Core\Models\Transaksi\TransaksiID;
use App\Core\Repository\TransaksiRepositoryInterface;
use Illuminate\Support\Facades\DB;

class TolakTransaksiCommand
{
    public function __construct(
        private TransaksiRepositoryInterface $transaksi_repository
    ) {
    }

    public function execute(SetujuiTransaksiRequest $request) {
        $transaksi = $this->transaksi_repository->byId(new TransaksiID($request->transaksi_id));
        if(!$transaksi) {
            throw new \Exception("Transaksi tidak ditemukan");
        }

        DB::beginTransaction();
        try {
            $transaksi->reject();
            $this->transaksi_repository->update($transaksi);
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
