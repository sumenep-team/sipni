<?php


namespace App\Application\Command\TolakTransaksi;


class TolakTransaksiRequest
{
    public function __construct(
        public string $transaksi_id
    )
    { }
}
