<?php


namespace App\Application\Command\BuatKursus;


class BuatKursusRequest
{
    public function __construct(
        public string $sanggar_id,
        public string $nama_kursus,
        public ?string $deskripsi,
        public ?string $tempat,
        public ?string $tag,
        public bool $is_dewasa,
        public ?string $foto_kursus,
    )
    { }
}
