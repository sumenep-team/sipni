<?php


namespace App\Application\Command\BuatKursus;


use App\Core\Models\Kursus\Kursus;
use App\Core\Models\Kursus\KursusID;
use App\Core\Models\Sanggar\SanggarID;
use App\Core\Repository\KursusRepositoryInterface;
use App\Core\Repository\SanggarRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Throwable;

class BuatKursusCommand
{
    public function __construct(
        private SanggarRepositoryInterface $sanggar_repository,
        private KursusRepositoryInterface $kursus_repository
    ) {
    }

    public function execute(BuatKursusRequest $request): void
    {
        $sanggar = $this->sanggar_repository->byId(new SanggarID($request->sanggar_id));
        if(!$sanggar) {
            throw new \Exception("Sanggar tidak ditemukan");
        }
        DB::beginTransaction();
        try {
            $kursus = new Kursus(
                new KursusID(Uuid::uuid4()->toString()),
                $request->nama_kursus,
                $request->deskripsi,
                $request->tempat,
                $request->tag,
                $request->is_dewasa,
                $request->foto_kursus
            );
            $this->kursus_repository->save($kursus, $sanggar->getSanggarID()->getId());
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
