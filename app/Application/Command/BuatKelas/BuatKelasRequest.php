<?php

namespace App\Application\Command\BuatKelas;

class BuatKelasRequest
{
    public function __construct(
        public string $kursus_id,
        public string $nama_kelas,
        public string $instruktur,
        public int $kapasitas,
        public int $harga
    )
    {
    }
}