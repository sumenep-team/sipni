<?php

namespace App\Application\Command\BuatKelas;

use App\Core\Models\Kelas\Kelas;
use App\Core\Models\Kelas\KelasID;
use App\Core\Models\Kelas\StatusKelas;
use App\Core\Models\Kursus\KursusID;
use App\Core\Models\Nominal\Nominal;
use App\Core\Repository\KelasRepositoryInterface;
use App\Core\Repository\KursusRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;
use Ramsey\Uuid\Uuid;
class BuatKelasCommand
{
    public function __construct(
        private KelasRepositoryInterface $kelas_repository,
        private KursusRepositoryInterface $kursus_repository
    ) {
    }

    public function execute(BuatKelasRequest $request)
    {
        $kursus = $this->kursus_repository->byId(new KursusID($request->kursus_id));
        if(!$kursus)
        {
            throw new \Exception("Kursus tidak ditemukan dalam db");
        }
        if($request->kapasitas < 0)
        {
            throw new \Exception("Kapasitas tidak boleh negatif");
        }
        if($request->harga < 0)
        {
            throw new \Exception("Harga tidak boleh negatif");
        }
        DB::beginTransaction();

        try{
            $kelas = new Kelas(
                new KelasID(Uuid::uuid4()->toString()),
                $kursus->getKursusId(),
                $request->nama_kelas,
                $request->instruktur,
                new StatusKelas(StatusKelas::BELUM_DIMULAI),
                $request->kapasitas,
                0,
                new Nominal($request->harga)
            );
            $this->kelas_repository->save($kelas);

        } catch(Throwable $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
