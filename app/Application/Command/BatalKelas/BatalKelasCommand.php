<?php

namespace App\Application\Command\BatalKelas;

use App\Core\Models\Kelas\KelasID;
use App\Core\Models\Pelajar\PelajarID;
use App\Core\Models\Sanggar\SanggarID;
use App\Core\Repository\KelasRepositoryInterface;
use App\Core\Repository\PelajarRepositoryInterface;
use App\Core\Repository\PesertaKelasRepositoryInterface;
use App\Core\Repository\SanggarRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;
use Ramsey\Uuid\Uuid;
class BatalKelasCommand
{
    public function __construct(
        private KelasRepositoryInterface $kelas_repository,
        private PelajarRepositoryInterface $pelajar_repository,
        private SanggarRepositoryInterface $sanggar_repository,
        private PesertaKelasRepositoryInterface $peserta_kelas_repository
    ) {
    }

    public function execute(BatalKelasRequest $request)
    {
        $kelas = $this->kelas_repository->byId(
            new KelasID($request->kelas_id)    
        );
        if(!$kelas)
        {
            throw new \Exception("kelas tidak ditemukan di database");
        }

        $sanggar = $this->sanggar_repository->byId(
            new SanggarID($request->sanggar_id)
        );
        if(!$sanggar)
        {
            throw new \Exception("sanggar tidak ditemukan");
        }

        $pelajar = $this->pelajar_repository->byId(
            new PelajarID($request->pelajar_id)
        );
        if(!$pelajar)
        {
            throw new \Exception("pelajar tidak ditemukan");
        }

        $peserta_kelas = $this->peserta_kelas_repository->byPelajarIDandKelasID(
            $pelajar->getPelajar_id(),
            $kelas->getKelas_id()
        );
        if(!$peserta_kelas)
        {
            throw new \Exception("peserta kelas bukan merupakan peserta kelas");
        }

        $pelajar->getDompet_pelajar()->tambahNominal($kelas->getHarga()->getNominalNomor());
        $sanggar->getDompet_sanggar()->kurangNominal($kelas->getHarga()->getNominalNomor());

        DB::beginTransaction();
        try
        {
            $this->pelajar_repository->update($pelajar);
            $this->sanggar_repository->update($sanggar);
            $this->peserta_kelas_repository->delete($peserta_kelas->getId());
            DB::commit();
        }
        catch(Throwable $exception)
        {
            DB::rollBack();
        }
        DB::commit();


    }
}
