<?php

namespace App\Application\Command\BatalKelas;

class BatalKelasRequest
{
    public function __construct(
        public string $pelajar_id,
        public string $sanggar_id,
        public string $kelas_id
    )
    {
    }
}