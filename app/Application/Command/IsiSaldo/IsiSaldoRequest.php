<?php


namespace App\Application\Command\IsiSaldo;


class IsiSaldoRequest
{
    public function __construct(
        public string $pelajar_id,
        public int $nominal,
        public string $foto_bukti_transaksi
    )
    { }
}
