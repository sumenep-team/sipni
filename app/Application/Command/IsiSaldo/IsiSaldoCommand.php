<?php


namespace App\Application\Command\IsiSaldo;


use App\Core\Models\Nominal\Nominal;
use App\Core\Models\Pelajar\PelajarID;
use App\Core\Models\Transaksi\Transaksi;
use App\Core\Models\Transaksi\TransaksiID;
use App\Core\Repository\PelajarRepositoryInterface;
use App\Core\Repository\TransaksiRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

class IsiSaldoCommand
{
    public function __construct(
        private PelajarRepositoryInterface $pelajar_repository,
        private TransaksiRepositoryInterface $transaksi_repository
    ) {
    }

    public function execute(IsiSaldoRequest $request) {
        $pelajar = $this->pelajar_repository->byId(new PelajarID($request->pelajar_id));
        if(!$pelajar) {
            throw new \Exception("Pelajar tidak ditemukan");
        }

        DB::beginTransaction();
        try {
            $transaksi = new Transaksi(
                new TransaksiID(Uuid::uuid4()->toString()),
                new PelajarID($request->pelajar_id),
                new Nominal($request->nominal),
                $request->foto_bukti_transaksi
            );
            $this->transaksi_repository->save($transaksi);
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
