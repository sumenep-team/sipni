<?php

namespace App\Application\Command\HapusWishlist;

class HapusWishlistRequest
{
    public function __construct(
        public string $pelajar_id,
        public string $kelas_id
    )
    {
    }
}