<?php

namespace App\Application\Command\HapusWishlist;

use App\Core\Models\Kelas\KelasID;
use App\Core\Models\Pelajar\PelajarID;
use App\Core\Repository\KelasRepositoryInterface;
use App\Core\Repository\PelajarRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

class HapusWishlistCommand
{
    public function __construct(
        private KelasRepositoryInterface $kelas_repository,
        private PelajarRepositoryInterface $pelajar_repository
    ) {
    }

    public function execute(HapusWishlistRequest $request)
    {
        $kelas = $this->kelas_repository->byId(
            new KelasID($request->kelas_id)    
        );
        if(!$kelas)
        {
            throw new \Exception("kelas tidak ditemukan di database");
        }

        $pelajar = $this->pelajar_repository->byId(
            new PelajarID($request->pelajar_id)
        );
        
        if(!$pelajar)
        {
            throw new \Exception("pelajar tidak ditemukan di database");
        }

        DB::beginTransaction();
        try{
            $wishlist = $pelajar->getWishlist();
            $wishlist->removeWishlist($kelas->getKelas_id());
            $this->pelajar_repository->update($pelajar);
        }
        catch(Throwable $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
