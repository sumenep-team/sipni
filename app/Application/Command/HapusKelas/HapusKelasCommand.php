<?php

namespace App\Application\Command\HapusKelas;

use App\Core\Models\Kelas\KelasID;
use App\Core\Repository\KelasRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Throwable;

class HapusKelasCommand
{
    public function __construct(
        private KelasRepositoryInterface $kelas_repository,
    ) {
    }

    public function execute(HapusKelasRequest $request)
    {
        $kelas = $this->kelas_repository->byId(
            new KelasID($request->kelas_id)    
        );
        if(!$kelas)
        {
            throw new \Exception("kelas tidak ditemukan di database");
        }

        if($kelas->getJumlah_peserta() > 0)
        {
            throw new \Exception("Kelas memiliki peserta.");
        }

        DB::beginTransaction();
        try{
            $this->kelas_repository->delete($kelas);
        }
        catch(Throwable $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
