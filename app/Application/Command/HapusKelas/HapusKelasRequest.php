<?php

namespace App\Application\Command\HapusKelas;

class HapusKelasRequest
{
    public function __construct(
        public string $kelas_id
    )
    {
    }
}