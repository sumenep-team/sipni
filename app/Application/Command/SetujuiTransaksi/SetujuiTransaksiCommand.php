<?php


namespace App\Application\Command\SetujuiTransaksi;


use App\Application\Command\IsiSaldo\IsiSaldoRequest;
use App\Core\Models\Nominal\Nominal;
use App\Core\Models\Pelajar\PelajarID;
use App\Core\Models\Transaksi\Transaksi;
use App\Core\Models\Transaksi\TransaksiID;
use App\Core\Repository\PelajarRepositoryInterface;
use App\Core\Repository\TransaksiRepositoryInterface;
use Illuminate\Support\Facades\DB;

class SetujuiTransaksiCommand
{
    public function __construct(
        private PelajarRepositoryInterface $pelajar_repository,
        private TransaksiRepositoryInterface $transaksi_repository
    ) {
    }

    public function execute(SetujuiTransaksiRequest $request) {
        $pelajar = $this->pelajar_repository->byId(new PelajarID($request->pelajar_id));
        if(!$pelajar) {
            throw new \Exception("Pelajar tidak ditemukan");
        }
        $transaksi = $this->transaksi_repository->byId(new TransaksiID($request->transaksi_id));
        if(!$transaksi) {
            throw new \Exception("Transaksi tidak ditemukan");
        }

        DB::beginTransaction();
        try {
            $transaksi->accept();
            $pelajar->getDompet_pelajar()->tambahNominal($transaksi->getTransaksiNominal()->getNominalNomor());
            $this->transaksi_repository->update($transaksi);
            $this->pelajar_repository->update($pelajar);
        } catch (Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
