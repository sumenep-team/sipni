<?php


namespace App\Application\Command\SetujuiTransaksi;


class SetujuiTransaksiRequest
{
    public function __construct(
        public string $pelajar_id,
        public string $transaksi_id
    )
    { }
}
