<?php

namespace App\Application\Command\TambahWishlist;

class TambahWishlistRequest
{
    public function __construct(
        public string $pelajar_id,
        public string $kelas_id
    )
    {
    }
}