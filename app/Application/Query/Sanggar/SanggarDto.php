<?php

namespace App\Application\Query\Sanggar;

class SanggarDto
{
    public function __construct(
        public string $id,
        public string $foto_profil,
        public string $nama,
        public string $username,
        public string $kota,
        public string $deskripsi,
    ) {}
}
