<?php

namespace App\Application\Query\Kelas;

class DaftarKelasDto
{
    public function __construct(
        public string $id,
        public string $nama,
        public string $instruktur,
        public int $status,
        public int $kapasitas
    ) {}
}
