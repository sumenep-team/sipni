<?php

namespace App\Application\Query\Kelas;

class KelasDto
{
    public function __construct(
        public string $id,
        public string $nama,
        public string $kursus_id,
        public string $kursus,
        public string $instruktur,
        public int $status,
        public int $kapasitas,
        public int $harga,
        public int $hari,
        public string $jam_mulai,
        public string $jam_selesai,
        public string $foto_kursus,
    ) {}
}