<?php


namespace App\Application\Query\Kelas;


interface DaftarKelasQueryInterface
{
    public function execute(string $kursus_id): ?array;
}
