<?php

namespace App\Application\Query\Kursus;

interface KursusQueryInterface
{
    public function execute(string $Id) : ?KursusDto;
}