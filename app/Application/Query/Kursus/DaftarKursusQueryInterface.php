<?php

namespace App\Application\Query\Kursus;

interface DaftarKursusQueryInterface
{
    public function execute(string $id_sanggar): ?array;
}
