<?php

namespace App\Application\Query\Kursus;

class KursusDto
{
    public function __construct(
        public string $id,
        public string $gambar,
        public string $nama,
        public string $deskripsi,
        public string $tempat,
        public string $tags,
        public string $is_dewasa
    ) {}
}
