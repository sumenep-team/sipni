<?php

namespace App\Providers;

use App\Application\Query\Kelas\DaftarKelasQueryInterface;
use App\Application\Query\Kelas\KelasQueryInterface;
use App\Application\Query\Pelajar\PelajarQueryInterface;
use App\Application\Query\Keranjang\KeranjangQueryInterface;
use App\Application\Query\Kursus\DaftarKursusQueryInterface;
use App\Application\Query\Kursus\KursusQueryInterface;
use App\Application\Query\Payment\PaymentQueryInterface;
use App\Application\Query\Sanggar\SanggarQueryInterface;
use App\Core\Repository\AdminRepositoryInterface;
use App\Core\Repository\KelasRepositoryInterface;
use App\Core\Repository\KursusRepositoryInterface;
use App\Core\Repository\PelajarRepositoryInterface;
use App\Core\Repository\PesertaKelasRepositoryInterface;
use App\Core\Repository\SanggarRepositoryInterface;
use App\Core\Repository\TransaksiRepositoryInterface;
use App\Infrastructure\Query\MySQL\DaftarKelasQuery;
use App\Infrastructure\Query\MySql\DaftarKursusQuery;
use App\Infrastructure\Query\MySql\KelasQuery;
use App\Infrastructure\Query\MySQL\PelajarQuery;
use App\Infrastructure\Query\MySQL\KeranjangQuery;
use App\Infrastructure\Query\MySQL\KursusQuery;
use App\Infrastructure\Query\MySQL\PaymentQuery;
use App\Infrastructure\Query\MySQL\SanggarQuery;
use App\Infrastructure\Repository\MySQL\AdminRepository;
use App\Infrastructure\Repository\MySQL\KursusRepository;
use App\Infrastructure\Repository\MySQL\KelasRepository;
use App\Infrastructure\Repository\MySQL\PelajarRepository;
use App\Infrastructure\Repository\MySQL\SanggarRepository;
use App\Infrastructure\Repository\MySQL\PesertaKelasRepository;
use App\Infrastructure\Repository\MySQL\TransaksiRepository;
use Illuminate\Support\ServiceProvider;

class DependencyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
        // TODO: Masukin dependencies disini

        // Query
        $this->app->bind(PelajarQueryInterface::class, PelajarQuery::class);
        $this->app->bind(KeranjangQueryInterface::class, KeranjangQuery::class);
        $this->app->bind(KursusQueryInterface::class, KursusQuery::class);
        $this->app->bind(PaymentQueryInterface::class, PaymentQuery::class);
        $this->app->bind(SanggarQueryInterface::class, SanggarQuery::class);
        $this->app->bind(DaftarKursusQueryInterface::class, DaftarKursusQuery::class);
        $this->app->bind(KursusQueryInterface::class, KursusQuery::class);
        $this->app->bind(KelasQueryInterface::class, KelasQuery::class);
        $this->app->bind(DaftarKelasQueryInterface::class, DaftarKelasQuery::class);
        //Repository
        $this->app->bind(AdminRepositoryInterface::class, AdminRepository::class);
        $this->app->bind(KelasRepositoryInterface::class, KelasRepository::class);
        $this->app->bind(KursusRepositoryInterface::class, KursusRepository::class);
        $this->app->bind(PelajarRepositoryInterface::class, PelajarRepository::class);
        $this->app->bind(PesertaKelasRepositoryInterface::class, PesertaKelasRepository::class);
        $this->app->bind(SanggarRepositoryInterface::class, SanggarRepository::class);
        $this->app->bind(TransaksiRepositoryInterface::class, TransaksiRepository::class);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
