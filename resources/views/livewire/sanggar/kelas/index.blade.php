<div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            @if(session('message'))
                <div class="alert alert-success">{{ session('message') }}</div>
            @endif
            {{--Content card start--}}
            <div class="card">
                <div class="card-header">
                    <h4>Kelas
                        <a href="{{ url('sanggar/kelas/buat') }}"
                           class="btn btn-primary btn-sm float-end text-white">Tambah kelas</a>
                    </h4>
                </div>
                <div class="card-body">
                    @foreach($kelasList as $kelas) {{--TODO: change variable categories to kelasList--}}
                    <!-- Card start -->
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row g-0">
                            <div class="col-md-4 bg-image hover-overlay ripple">
                                <img src="{{ asset('/uploads/category/' . $kelas->foto_kursus) }}" class="img-fluid"/> {{--TODO: change directory to kelas--}}
                                <a href="#!">
                                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                                </a>
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $kelas->nama }}</h5>
                                    <div class="container mb-3">
                                        <div class="row mb-2">
                                            <div class="col-sm-4 d-flex align-content-start">Instruktur</div>
                                            <div class="col-sm-8 d-flex align-content-start">{{ $kelas->instruktur }} {{--TODO: [kelas] change to actual variable--}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 d-flex align-content-start">Peserta</div>
                                            <div class="col-sm-8 d-flex align-content-start">{{ $kelas->kapasitas }} {{--TODO: [kelas] change to actual variable--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row align-items-center">
                                            <div class="col">
                                                    <span
                                                        class="badge d-flex align-items-center p-1 pe-2 text-info-emphasis bg-info border border-info-subtle rounded-pill">
                                                        <span class="me-1" width="24" height="24" style="background-color: blue
                                                        {{--@switch($kelas->status->getId())
                                                            @case(1)
                                                                blue
                                                                @break

                                                            @case(2)
                                                                red
                                                                @break

                                                            @default
                                                                grey
                                                        @endswitch TODO: [kelas] uncomment if variable already available--}}
                                                        "></span>
                                                        @switch($kelas->status)
                                                            @case(1)
                                                                Belum dimulai
                                                                @break

                                                            @case(2)
                                                                Sedang berlangsung
                                                                @break

                                                            @case(3)
                                                                Selesai
                                                                @break

                                                            @default
                                                                <option selected>Pilih bahasa</option>
                                                                <option value="0">Indonesia</option>
                                                                <option value="1">English</option>
                                                        @endswitch
                                                    </span>
                                            </div>
                                            <div class="col">
                                                <a href="{{ url('sanggar/kelas/'.$kelas->id.'/details') }}"
                                                   class="btn btn-secondary">Lihat detail</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Card end -->
                    @endforeach
                </div>
            </div>
            {{--Content card end--}}
        </div>
    </div>
</div>

@push('script')
    <script>
        window.addEventListener('close-modal', event => {
            $('#detailModal').modal('hide');
        })
    </script>
@endpush
