@php
    class ProfilSanggarDtoFake {
        public function __construct(
            public string $id,
            public string $foto_profil,
            public string $nama,
            public string $username,
            public string $kota,
            public string $deskripsi,
        ) {}
    }
    $profil_sanggar = new ProfilSanggarDtoFake(
        6,
        '1687175321.jpg',
        'Sanggar Wiraga Ayu',
        '@wiraga_ayu',
        'Surabaya',
        '-',
    );
@endphp
<div>

    <div class="row">
        <div class="col-md-12 grid-margin">
            @if(session('message'))
                <div class="alert alert-success">{{ session('message') }}</div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4>Profil Sanggar
                        <a href="{{ url('sanggar/profil/'.$profil_sanggar->id.'/ubah') }}"
                           class="btn btn-success">Edit<i class="bi bi-pencil text-sm ms-2"></i></a>
                    </h4>
                </div>
                <div class="card-body">
                    <!-- Card start -->
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row-md-4 bg-image hover-overlay ripple">

                        </div>
                        <div class="row-md-8">
                            <div class="card-body">
                                <div class="container mb-3">
                                    <div class="row mb-2">
                                        <img src="{{ asset('/uploads/profil/' . $profil_sanggar->foto_profil) }}" class="rounded-circle object-fit-cover p-0"
                                             style="object-fit: cover; height: 150px; width: 150px"/>
                                        {{-- TODO: change directory to kelas --}}
                                        <a href="#!">
                                            <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                                        </a>
                                    </div>
                                    <h5 class="row mt-3 mb-3 fs-4 fw-bolder">
                                        <div class="col-sm-auto d-flex align-content-start">{{ $profil_sanggar->nama }}</div>
                                        <div class="col-sm-auto d-flex align-content-start text-gray">{{ $profil_sanggar->username }}{{-- TODO: [kelas] change to actual variable --}}
                                        </div>
                                    </h5>
                                    <div class="row mb-2">
                                        <div class="col-sm-4 d-flex fw-bolder align-content-start">Kota</div>
                                        <div class="col-sm-8 d-flex align-content-start">{{ $profil_sanggar->kota }}{{-- TODO: [kelas] change to actual variable --}}</div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-sm-4 d-flex fw-bolder align-content-start">Deskripsi</div>
                                        <div class="col-sm-8 d-flex align-content-start">{{ $profil_sanggar->deskripsi }}{{-- TODO: [kelas] change to actual variable --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Card end -->
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script>
        window.addEventListener('close-modal', event => {
            $('#deleteModal').modal('hide');
        })
    </script>
@endpush
