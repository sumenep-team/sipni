<div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            @if(session('message'))
                <div class="alert alert-success">{{ session('message') }}</div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4>Kursus
                        <a href="{{ url('sanggar/kursus/buat') }}"
                           class="btn btn-primary btn-sm float-end text-white">Tambah kursus</a>
                    </h4>
                </div>
                <div class="card-body">
                    @foreach($kursusList as $kursus)
                        <!-- Card start -->
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row g-0">
                                <div class="col-md-4 bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                    <img src="{{ asset('/uploads/kursus/' . $kursus->gambar) }}" class="img-fluid" />
                                    <a href="#!">
                                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                                    </a>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $kursus->nama }}</h5>
                                        <p class="card-text">{{ $kursus->deskripsi }}</p>
                                        <p class="card-text">Tags</p>
                                        <a href="{{ url('sanggar/kursus/'.$kursus->id.'/ubah') }}"
                                           class="btn btn-success">Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Card end -->
                    @endforeach
{{--                    <div>--}}
{{--                        {{ $categories->links() }}--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script>
        window.addEventListener('close-modal', event => {
            $('#deleteModal').modal('hide');
        })
    </script>
@endpush
