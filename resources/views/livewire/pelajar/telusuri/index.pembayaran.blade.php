@php
    class TransaksiDtoFake {
        public function __construct(
            public string $id,
            public int $nominal,
            public string $fotobukti,
            public bool $status,
        ) {}
    }
    $transaksi_saya = new TransaksiDtoFake(
        6,
        500000,
        '1111.jpg',
        True
    );
@endphp

<div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalToggleLabel">Selesaikan Top-up Anda!</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Transfer to: 1512389071230 BSI a.n PT. Sipni.
                <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                <div class="col-sm-10">
                    <div class="input-group mb-3">
                        <span class="input-group-text">Rp</span>
                        <input name="harga" type="text" class="form-control"
                               aria-label="Amount (to the nearest rupiah)" value="{{ $transaksi_saya->nominal }}"
                            {{--                               @if($kelas->stasus == 0)--}}
                            {{--                               @else--}}
                            {{--                                   disabled--}}
                            {{--                        @endif--}}
                        >
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text">Link</span>
                        <input name="harga" type="text" class="form-control"
                               aria-label="Amount (to the nearest rupiah)" value="{{ $transaksi_saya->fotobukti }}"
                            {{--                               @if($kelas->stasus == 0)--}}
                            {{--                               @else--}}
                            {{--                                   disabled--}}
                            {{--                        @endif--}}
                        >
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal">Upload Bukti</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalToggleLabel2">Upload Berhasil</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Oke"></button>
            </div>
            <div class="modal-body">
                <p>
                    Tunggu verifikasi dari admin kami ya.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Oke</button>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-primary" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">Pay With Wallet</button>
<a class="btn btn-secondary btn-rounded">other methods coming soon</a>
