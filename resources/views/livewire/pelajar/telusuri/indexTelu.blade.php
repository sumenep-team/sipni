<div>
    <h2 class="text-center">
        <span style="color: green;">Taklukkan daerah</span> yang kau ingin melalui kelas-kelas yang tersedia!
    </h2>
    <hr>

    <h4>Sanggar</h4>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($sanggarList as $sanggar)
            <div class="col">
                <div class="card h-100">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$sanggar->nama}}</h5>
                        <p class="card-text">{{$sanggar->kota}}</p>
                        <div class="text-end">
                            <a href="{{ url('pelajar/detailsanggar/'.$sanggar->id) }}"
                               class="btn btn-success btn-rounded" style="font-size: 10px">Telusuri</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <hr>
    <h4>Kelas</h4>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($kelasList as $kelas)
            <div class="col">
                <div class="card h-100">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$kelas->nama}}</h5>
                        <p class="card-text">{{$kelas->kapasitas}}</p>
                        <div class="text-end">
                            <a href="{{ url('pelajar/detailkelas/'.$kelas->id) }}"
                               class="btn btn-success btn-rounded" style="font-size: 10px">Telusuri</a>
                            <a href="{{ url('admin/category/'.$kelas->id.'/edit') }}"
                               class="btn btn-secondary btn-rounded" style="font-size: 10px">Add to Wishlist</a>
                            <!--TODO: wishlist route-->
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
