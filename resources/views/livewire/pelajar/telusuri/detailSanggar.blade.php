<?php

@php
    class ProfilSanggarDtoFake {
        public function __construct(
            public string $id,
            public string $foto_profil,
            public string $nama,
            public string $username,
            public string $kota,
            public string $deskripsi,
        ) {}
    }
    $profil_sanggar = new ProfilSanggarDtoFake(
        6,
        '1687175321.jpg',
        'Sanggar Wiraga Ayu',
        '@wiraga_ayu',
        'Surabaya',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu cursus turpis, non aliquam elit. Suspendisse posuere euismod placerat. Sed ac nisl mauris. Curabitur vitae efficitur libero, non dignissim mauris. Nulla vehicula leo semper, dapibus tortor at, finibus sem. In ut ipsum metus. Duis sit amet nulla id est dictum rutrum sodales quis magna. Proin in tortor mollis, pellentesque metus sed, scelerisque arcu. Pellentesque vel mattis erat. Etiam vehicula enim quam. Aenean pretium ante orci, id lacinia purus condimentum sed.
        Sed vitae commodo ligula. Quisque vitae turpis turpis. Suspendisse at massa libero. Aliquam erat volutpat. Nullam velit augue, tincidunt quis urna imperdiet, finibus sodales tortor. Sed eu mattis neque. Donec convallis erat vitae erat pulvinar, ac consectetur tellus luctus. Mauris quis ex in enim interdum consequat quis vitae risus. Ut accumsan porta sem, dignissim lobortis lorem tempor cursus. In cursus faucibus nunc accumsan viverra. Ut finibus, lacus vitae auctor scelerisque, nibh nisl luctus sem, ut tempor mauris mauris a dolor. Curabitur lacus diam, tincidunt eget libero eget, porta faucibus est.',
    );
    $profil_sanggar2 = new ProfilSanggarDtoFake(
        7,
        '112121212.jpg',
        'Sanggar Arya Widia',
        '@awia',
        'bli',
        'siap bersedia',
    );

    $profil_sanggar3 = new ProfilSanggarDtoFake(
        8,
        '23232323.jpg',
        'Sanggar Putro Widia',
        '@awiawi',
        'blibli',
        'sehat bersedia',
    );
    $sanggarList = [$profil_sanggar, $profil_sanggar2, $profil_sanggar3];
class KelasDtoFake {
        public function __construct(
            public string $id,
            public string $gambar,
            public string $nama,
            public string $kursus_id,
            public string $kursus,
            public string $instruktur,
            public int $stasus,
            public string $kapasitas,
            public string $harga,
            public int $hari,
            public string $jamMulai,
            public string $jamSelesai,
        ) {}
    }
    $kelas = new KelasDtoFake(
        6,
        '1687175321.jpg',
        'Tari Baris Dasar Juni 2023',
        7,
        'Tari Baris Dasar',
        'Sri Utami, S.Sn',
        1,
        10,
        100000,
        1,
        '12:00',
        '16:00',
    );
    $kelas2 = new KelasDtoFake(
        7,
        '123123123.jpg',
        'Bocil Kematian Anthem 2023',
        7,
        'Bocil Kematian Anthem',
        'Arya Widia, S.kom.',
        1,
        10,
        100000,
        1,
        '12:00',
        '16:00',
    );
    $kelasList = [$kelas, $kelas2];

    class KursusDtoFake {
        public function __construct(
            public string $id,
            public string $gambar,
            public string $nama,
            public string $deskripsi,
            public string $tempat,
            public string $tags,
        ) {}
    }
    $kursusItem = new KursusDtoFake(
        6,
        '1687175321.jpg',
        'Tari Baris Dasar',
        'Tari Baris Dasar deskripsi',
        'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
        'Tari, Tari Tradisional, Bali',
    );
    $kursusItem2 = new KursusDtoFake(
        7,
        '1687175321.jpg',
        'Tari Baris Dasar New',
        'Tari Baris Dasar deskripsi',
        'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
        'Tari, Tari Tradisional, Bali',
    );
    $kursusList = [$kursusItem, $kursusItem2];
@endphp

<div class="card mb-3 rounded-bottom rounded-top" style="max-width: 800px; max-height: fit-content">
    <div class="row g-0">
        <div class="col-md-4">
            <img src="..." class="img-fluid rounded-start" alt="...">
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title">{{$profil_sanggar->nama}}</h5>
                <p class="card-text">{{$profil_sanggar->deskripsi}}</p>
                <a href="{{ url('pelajar/sanggar/'.$profil_sanggar->id.'/daftarkelas') }}"
                       class="btn btn-success btn-rounded">Telusuri</a>
                </div>
            </div>
        </div>
    </div>
