<div class="row">
    <div class="col-md-12 grid-margin">
        @if(session('message'))
            <div class="alert alert-success">{{ session('message') }}</div>
        @endif
        <div class="card-body">
            <div class="row">
                <h5 class="col text-center">
                    <a href="">
                        Sedang Berjalan
                    </a>
                </h5>
                <h5 class="col text-center">
                    <a href="">
                        Lampau
                    </a>
                </h5>
            </div>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Nama kursus</th>
                    <th>Jadwal</th>
                    <th>Sanggar</th>
                    <th>Tgl Selesai</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $kelas)
                    <tr>
                        <td>{ $kelas->name }}</td>
                        <td>{ $kelas->jadwal }}</td>
                        <td>{ $kelas->sanggar }}</td>
                        <td>{ $kelas->tgl selesai }}</td>
                        <td class="btn btn-rounded btn-secondary">Batal Kelas</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div>
                {{ $categories->links() }}
            </div>
            <br>
            <div>
                <h5 class="col text-center">
                    Asah Kemampuan Seni Lainnya: <a href="#"
                                                    class="btn btn-success rounded-pill ml-2 text-white fw-bold">Cari
                        Sanggar</a>
                </h5>
            </div>
        </div>
    </div>
</div>
