@php
    class AkunSayaDtoFake {
        public function __construct(
            public string $id,
            public string $nama,
            public string $username,
            public string $email,
            public int $lang,
        ) {}
    }
    $akun_saya = new AkunSayaDtoFake(
        6,
        'Arya Widia',
        '@awia',
        'awia@smelly.com',
        0,
    );
@endphp
<div>
    <div class="row">
        <div class="col-md-12 grid-margin">
            @if(session('message'))
                <div class="alert alert-success">{{ session('message') }}</div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4>Akun Saya
                        <a href="{{ url('pelajar/akun/'.$akun_saya->id.'/ubah') }}"
                           class=""><i class="bi bi-pencil text-sm ms-2 text-black"></i></a>
                    </h4>
                </div>
                <div class="card-body">
                    <!-- Card start -->
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row-md-8">
                            <div class="card-body">
                                <div class="container mb-3">
                                    <h5 class="row mt-3 mb-3 fs-4 fw-bolder">
                                        <div class="col-sm-auto d-flex align-content-start">{{ $akun_saya->nama }}</div>
                                        <div class="col-sm-auto d-flex align-content-start text-gray">{{ $akun_saya->username }}{{-- TODO: [kelas] change to actual variable --}}
                                        </div>
                                    </h5>
                                    <div class="row mb-2">
                                        <div class="col-sm-4 d-flex fw-bolder align-content-start">Email</div>
                                        <div class="col-sm-8 d-flex align-content-start">{{ $akun_saya->email }}{{-- TODO: [kelas] change to actual variable --}}</div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-sm-4 d-flex fw-bolder align-content-start">Bahasa</div>
                                        <div class="col-sm-8 d-flex align-content-start">{{ $akun_saya->lang == 0 ? 'Indonesia' : 'English' }}{{-- TODO: [kelas] change to actual variable --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Card end -->
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')

@endpush
