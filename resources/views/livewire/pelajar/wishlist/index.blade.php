@php
    class ProfilSanggarDtoFake {
        public function __construct(
            public string $id,
            public string $foto_profil,
            public string $nama,
            public string $username,
            public string $kota,
            public string $deskripsi,
        ) {}
    }
    $profil_sanggar = new ProfilSanggarDtoFake(
        6,
        '1687175321.jpg',
        'Sanggar Wiraga Ayu',
        '@wiraga_ayu',
        'Surabaya',
        '-',
    );
    $profil_sanggar2 = new ProfilSanggarDtoFake(
        7,
        '112121212.jpg',
        'Sanggar Arya Widia',
        '@awia',
        'bli',
        'siap bersedia',
    );

    $profil_sanggar3 = new ProfilSanggarDtoFake(
        8,
        '23232323.jpg',
        'Sanggar Putro Widia',
        '@awiawi',
        'blibli',
        'sehat bersedia',
    );
    $sanggarList = [$profil_sanggar, $profil_sanggar2, $profil_sanggar3];
class KelasDtoFake {
        public function __construct(
            public string $id,
            public string $gambar,
            public string $nama,
            public string $kursus_id,
            public string $kursus,
            public string $instruktur,
            public int $stasus,
            public string $kapasitas,
            public string $harga,
            public int $hari,
            public string $jamMulai,
            public string $jamSelesai,
        ) {}
    }
    $kelas = new KelasDtoFake(
        6,
        '1687175321.jpg',
        'Tari Baris Dasar Juni 2023',
        7,
        'Tari Baris Dasar',
        'Sri Utami, S.Sn',
        1,
        10,
        100000,
        1,
        '12:00',
        '16:00',
    );
    $kelas2 = new KelasDtoFake(
        7,
        '123123123.jpg',
        'Bocil Kematian Anthem 2023',
        7,
        'Bocil Kematian Anthem',
        'Arya Widia, S.kom.',
        1,
        10,
        100000,
        1,
        '12:00',
        '16:00',
    );
    $kelasList = [$kelas, $kelas2];

    class KursusDtoFake {
        public function __construct(
            public string $id,
            public string $gambar,
            public string $nama,
            public string $deskripsi,
            public string $tempat,
            public string $tags,
        ) {}
    }
    $kursusItem = new KursusDtoFake(
        6,
        '1687175321.jpg',
        'Tari Baris Dasar',
        'Tari Baris Dasar deskripsi',
        'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
        'Tari, Tari Tradisional, Bali',
    );
    $kursusItem2 = new KursusDtoFake(
        7,
        '1687175321.jpg',
        'Tari Baris Dasar New',
        'Tari Baris Dasar deskripsi',
        'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
        'Tari, Tari Tradisional, Bali',
    );
    $kursusList = [$kursusItem, $kursusItem2];
@endphp

<div>
    <h1 class="text-center">Wishlist
    </h1>
    <br>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($kelasList as $kelas)
            <div class="col">
                <div class="card h-100">
                    <img src="..." class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$kelas->nama}}</h5>
                        <p class="card-text">{{$kelas->kapasitas}}</p>
                        <div class="text-end">
                            <a href="{{ url('pelajar/kelas/'.$kelas->id) }}"
                               class="btn btn-success btn-rounded" style="font-size: 10px">Telusuri</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
