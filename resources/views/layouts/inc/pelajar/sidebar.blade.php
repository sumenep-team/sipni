<nav class="sidebar sidebar-offcanvas bg-primary" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link text-bg-dark" href="{{ url('pelajar/dashboard') }}">
                <i class="bi bi-house me-3"></i>
                <span class="menu-title">Beranda</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-bg-dark" href="{{ url('pelajar/kelas') }}">
                <i class="bi bi-journal-bookmark-fill me-3"></i>
                <span class="menu-title">Kelas Saya</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-bg-dark" href="{{ url('pelajar/telusuri') }}">
                <i class="bi bi-search me-3"></i>
                <span class="menu-title">Telusuri Kursus Baru</span>
            </a>
        </li>
    </ul>
</nav>
