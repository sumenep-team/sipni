<nav class="sidebar sidebar-offcanvas bg-secondary" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('sanggar/beranda') }}">
                <i class="bi bi-house me-3"></i>
                <span class="menu-title">Beranda</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('sanggar/kursus') }}">
                <i class="bi bi-journal-bookmark-fill me-3"></i>
                <span class="menu-title">Kursus</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('sanggar/kelas') }}">
                <i class="bi bi-table me-3"></i>
                <span class="menu-title">Kelas</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('sanggar/category') }}">
                <i class="bi bi-person-video3 me-3"></i>
                <span class="menu-title">Pengajar</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('sanggar/kursus/buat') }}">
                <i class="bi bi-plus-lg me-3"></i>
                <span class="menu-title">Buka Kursus Baru</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('sanggar/category') }}">
                <i class="mdi mdi-home menu-icon me-3"></i>
                <span class="menu-title">Categories</span>
            </a>
        </li>
    </ul>
</nav>
