<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sipni') }}</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('sanggar/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('sanggar/vendors/base/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('sanggar/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('sanggar/css/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('sanggar/images/favicon.png') }}"/>
    @livewireStyles
</head>
<body>
<div class="container-scroller">
    @include('layouts.inc.pelajar.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('layouts.inc.pelajar.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                @yield('content')

            </div>
        </div>
    </div>
</div>

<!-- container-scroller -->

<!-- plugins:js -->
<script src="{{ asset('sanggar/vendors/base/vendor.bundle.base.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{ asset('sanggar/vendors/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('sanggar/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('sanggar/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('sanggar/js/off-canvas.js') }}"></script>
<script src="{{ asset('sanggar/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('sanggar/js/template.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ asset('sanggar/js/dashboard.js') }}"></script>
<script src="{{ asset('sanggar/js/data-table.js') }}"></script>
<script src="{{ asset('sanggar/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('sanggar/js/dataTables.bootstrap4.js') }}"></script>
<!-- End custom js for this page-->

<script src="js/jquery.cookie.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
@livewireScripts
@stack('script')
</body>
</html>
