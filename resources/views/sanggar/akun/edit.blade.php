{{--TODO: delete this php code below if Kelas object has been connected arya bau--}}
@php
    class AkunSanggarDtoFake {
    public function __construct(
        public string $id,
        public string $nama,
        public string $username,
        public string $email,
        public int $lang,
    ) {}
}
$akun_sanggar = new AkunSanggarDtoFake(
    6,
    'Sanggar Wiraga Ayu',
    '@wiraga_ayu',
    'admin_wiragaayu@example.com',
    0,
);
@endphp
@extends('layouts.sanggar')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card rounded bg-primary text-white">
                <div class="card-header">
                    <h4>Pengaturan Akun Sanggar{{--TODO: change to sanggar/kelas--}}
                        <a href="{{ url('sanggar/akun') }}" class="btn btn-danger btn-sm float-end text-white">Batal<i
                                class="bi bi-x-lg text-sm ms-2"></i></a> {{--Ini bisa diganti return atau lainnya kek di mockup--}}
                    </h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('sanggar/kursus/'.$akun_sanggar->id) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row mb-3">
                            <label for="email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" class="form-control" id="email"
                                       value="{{ $akun_sanggar->email }}">
                            </div>
                            @error('email')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="bahasa" class="col-sm-2 col-form-label">Bahasa</label>
                            <div class="col-sm-10">
                                <select class="form-select" name="bahasa" aria-label="Default select example">
                                    @switch($akun_sanggar->lang)
                                        @case(0)
                                            <option>Pilih bahasa</option>
                                            <option value="0" selected>Indonesia</option>
                                            <option value="1">English</option>
                                            @break

                                        @case(1)
                                            <option>Pilih bahasa</option>
                                            <option value="0">Indonesia</option>
                                            <option value="1" selected>English</option>
                                            @break

                                        @default
                                            <option selected>Pilih bahasa</option>
                                            <option value="0">Indonesia</option>
                                            <option value="1">English</option>
                                    @endswitch
                                </select>
                            </div>
                            @error('bahasa')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <button type="submit" class="btn btn-primary float-end text-white">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
