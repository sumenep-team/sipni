{{--TODO: delete this php code below if Kelas object has been connected--}}
@php
    class ProfilSanggarDtoFake {
        public function __construct(
            public string $id,
            public string $foto_profil,
            public string $nama,
            public string $username,
            public string $kota,
            public string $deskripsi,
        ) {}
    }
    $profil_sanggar = new ProfilSanggarDtoFake(
        6,
        '1687175321.jpg',
        'Sanggar Wiraga Ayu',
        '@wiraga_ayu',
        'Surabaya',
        '-',
    );
@endphp
@extends('layouts.sanggar')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card rounded bg-primary text-white">
                <div class="card-header">
                    <h4>Edit Profil {{--TODO: change to sanggar/kelas--}}
                        <a href="{{ url('sanggar/profil') }}" class="btn btn-danger btn-sm float-end text-white">Batal<i
                                class="bi bi-x-lg text-sm ms-2"></i></a> {{--Ini bisa diganti return atau lainnya kek di mockup--}}
                    </h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('sanggar/profil/'.$profil_sanggar->id) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row mb-3">
                            <img src="{{ asset('/uploads/profil/' . $profil_sanggar->foto_profil) }}"
                                 class="rounded-circle object-fit-cover p-0"
                                 style="object-fit: cover; height: 150px; width: 150px"/>
                            <div class="col-sm-10">
                                <label for="foto_profil" class="col-sm-2 col-form-label">Gambar</label>
                                <input type="file" id="foto_profil" name="foto_profil"
                                       class="form-control col-sm-8 d-flex align-content-start" id=""/>
                            </div>
                            @error('foto_profil')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="nama" class="col-sm-2 col-form-label">Nama Sanggar</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" class="form-control" id="nama"
                                       value="{{ $profil_sanggar->nama }}">
                            </div>
                            @error('nama')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="username" class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" name="username" class="form-control" id="username"
                                       value="{{ $profil_sanggar->username }}">
                            </div>
                            @error('username')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <input type="text" name="deskripsi" class="form-control" id="deskripsi"
                                       value="{{ $profil_sanggar->deskripsi }}">
                            </div>
                            @error('deskripsi')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="kota" class="col-sm-2 col-form-label">Kota</label>
                            <div class="col-sm-10">
                                <input type="text" name="kota" class="form-control" id="kota"
                                       value="{{ $profil_sanggar->kota }}">
                            </div>
                            @error('kota')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <button type="submit" class="btn btn-primary float-end text-white">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
