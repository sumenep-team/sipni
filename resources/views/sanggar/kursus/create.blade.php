{{--TODO: delete this php code below if Kursus object has been connected--}}
@php
    class KursusDtoFake {
        public function __construct(
            public string $id,
            public string $gambar,
            public string $nama,
            public string $deskripsi,
            public string $tempat,
            public string $tags,
        ) {}
    }
    $kursus = new KursusDtoFake(
        6,
        '1687175321.jpg',
        'Tari Baris Dasar',
        'Tari Baris Dasar deskripsi',
        'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
        'Tari, Tari Tradisional, Bali',
    );

@endphp
@extends('layouts.sanggar')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card rounded-3 bg-primary text-white">
                <div class="card-header">
                    <h4>Buat Kursus {{--TODO: change to sanggar/category--}}
                        <a href="{{ url('sanggar/kursus') }}" class="btn btn-primary btn-sm float-end text-white">Kembali
                            ke Kelola<i
                                class="bi bi-arrow-left text-sm ms-2"></i></a> {{--Ini bisa diganti return atau lainnya kek di mockup--}}
                    </h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('sanggar/kursus') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <label for="gambar" class="col-sm-2 col-form-label">Gambar</label>
                            <div class="col-sm-10">
                                <input type="file" name="gambar"
                                       class="form-control col-sm-8 d-flex align-content-start" id=""/>
                            </div>
                            @error('gambar')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="nama_kursus" class="col-sm-2 col-form-label">Nama Kursus</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama_kursus" class="form-control" id="nama_kursus">
                            </div>
                            @error('nama_kursus')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="deskripsi" id=""
                                          rows="10"></textarea>
                            </div>
                            @error('deskripsi')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="tags" class="col-sm-2 col-form-label">Tags</label>
                            <div class="col-sm-10">
                                <input type="text" name="tags" class="form-control" id="tags">
                            </div>
                            @error('tags')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="nama_kursus" class="col-sm-2 col-form-label">Tempat</label>
                            <div class="col-sm-10">
                                <input type="text" name="tempat" class="form-control" id="nama_kursus">
                            </div>
                            @error('tempat')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-2 col-form-label">Terbatas Umur</div>
                            <div class="form-check col-sm-10">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" name="isDewasa">
                                <label class="form-check-label" for="flexCheckChecked">
                                    Checked checkbox
                                </label>
                            </div>
                            @error('tempat')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <button type="submit" class="btn btn-primary float-end text-white">Buat</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
