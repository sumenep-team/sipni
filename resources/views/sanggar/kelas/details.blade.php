{{--TODO: Connect to pelajar info, especially on Kirim pesan and Details--}}

@extends('layouts.sanggar')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4>Detail Kelas
                        <a href="{{ url('sanggar/kelas') }}" class="btn btn-primary btn-sm float-end text-white">Kembali
                            ke Kelola</a> {{-- Ini bisa diganti return atau lainnya kek di mockup --}}
                    </h4>
                </div>
                <div class="card-body">
                    <!-- Card start -->
                    <div class="row">
                        <h5 class="card-title">{{ $kelas->nama }}
                            <a href="{{ url('sanggar/kelas/' . $kelas->id . '/ubah') }}"
                               class="btn btn-secondary">Edit</a>
                        </h5>
                    </div>
                    <div class="card mb-3" style="max-width: 540px;">
                        <div class="row-md-4 bg-image hover-overlay ripple">
                            <img src="{{ asset('/uploads/kelas/' . $kelas->foto_kursus) }}" class="img-fluid"/>
                            {{-- TODO: change directory to kelas --}}
                            <a href="#!">
                                <div class="mask" style="background-color: rgba(251, 251, 251, 0.15)"></div>
                            </a>
                        </div>
                        <div class="row-md-8">
                            <div class="card-body">
                                <div class="container mb-3">
                                    <div class="row mb-2">
                                        <div class="col-sm-4 d-flex align-content-start">Instruktur</div>
                                        <div class="col-sm-8 d-flex align-content-start">{{ $kelas->instruktur }} {{-- TODO: [kelas] change to actual variable --}}
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-sm-4 d-flex align-content-start">Peserta</div>
                                        <div class="col-sm-8 d-flex align-content-start">
                                            <div class="row">
                                                {{ $kelas->kapasitas}} {{-- TODO: [kelas] change to actual variable --}}
                                                <table class="table table-bordered table-striped mt-2">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Nama Peserta</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($listPelajar as $pelajar)
                                                        <tr>
                                                            <td>{{ $pelajar->id }}</td>
                                                            <td>{{ $pelajar->nama }}</td>
                                                            <td>
                                                                <a href="{{ url('sanggar/kelas/' . $pelajar->id . '/ubah') }}"
                                                                   class="btn btn-primary">Kirim pesan</a>
                                                                <a href="{{ url('sanggar/kelas/' . $pelajar->id . '/ubah') }}"
                                                                   class="btn btn-secondary">Lihat detail</a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 d-flex align-content-start">Status</div>
                                        <div class="col-sm-8 d-flex align-content-start"><span
                                                class="badge d-flex align-items-center p-1 pe-2 text-info-emphasis bg-info border border-info-subtle rounded-pill">
                                                <span class="me-1" width="24" height="24"
                                                      style="background-color:
                                            @switch($kelas->status)
                                                @case(1)
                                                    blue
                                                    @break

                                                @case(2)
                                                    red
                                                    @break

                                                @default
                                                    grey
                                            @endswitch
                                            /*TODO: [kelas] uncomment if variable already available --}}*/
                                            "></span>
                                                @switch($kelas->status)
                                                    @case(1)
                                                        Belum dimulai
                                                        @break

                                                    @case(2)
                                                        Sedang berlangsung
                                                        @break

                                                    @case(3)
                                                        Selesai
                                                        @break

                                                    @default
                                                        <option selected>Pilih bahasa</option>
                                                        <option value="0">Indonesia</option>
                                                        <option value="1">English</option>
                                                @endswitch
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Card end -->
                </div>
            </div>
        </div>
    </div>
@endsection
