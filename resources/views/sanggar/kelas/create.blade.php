{{--TODO: delete this php code below if Kelas object has been connected--}}
@php
    class KelasDtoFake {
        public function __construct(
            public string $id,
            public string $gambar,
            public string $nama,
            public string $kursus_id,
            public string $kursus,
            public string $instruktur,
            public int $stasus,
            public string $kapasitas,
            public string $harga,
            public int $hari,
            public string $jamMulai,
            public string $jamSelesai,
        ) {}
    }
    $kelas = new KelasDtoFake(
        6,
        '1687175321.jpg',
        'Tari Baris Dasar Juni 2023',
        7,
        'Tari Baris Dasar',
        'Sri Utami, S.Sn',
        1,
        10,
        100000,
        1,
        '12:00',
        '16:00',
    );

    class KursusDtoFake {
        public function __construct(
            public string $id,
            public string $gambar,
            public string $nama,
            public string $deskripsi,
            public string $tempat,
            public string $tags,
        ) {}
    }
    $kursusItem = new KursusDtoFake(
        6,
        '1687175321.jpg',
        'Tari Baris Dasar',
        'Tari Baris Dasar deskripsi',
        'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
        'Tari, Tari Tradisional, Bali',
    );
    $kursusItem2 = new KursusDtoFake(
        7,
        '1687175321.jpg',
        'Tari Baris Dasar New',
        'Tari Baris Dasar deskripsi',
        'https://goo.gl/maps/QLC2oUUhERjH8LVU6',
        'Tari, Tari Tradisional, Bali',
    );
    $kursusList = [$kursusItem, $kursusItem2];

@endphp
@extends('layouts.sanggar')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card rounded bg-primary text-white">
                <div class="card-header">
                    <h4>Buat Kelas {{--TODO: change to sanggar/kelas--}}
                        <a href="{{ url('sanggar/kelas') }}" class="btn btn-primary btn-sm float-end text-white">Kembali
                            ke Kelola<i
                                class="bi bi-arrow-left text-sm ms-2"></i></a> {{--Ini bisa diganti return atau lainnya kek di mockup--}}
                    </h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('sanggar/kelas') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <label for="nama" class="col-sm-2 col-form-label">Nama Kelas</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" class="form-control" id="nama">
                            </div>
                            @error('nama')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="kursus" class="col-sm-2 col-form-label">Kursus</label>
                            <div class="col-sm-10">
                                <select class="form-select" name="kursus" aria-label="Default select example">
                                    <option selected>Pilih kursus</option>
                                    @foreach($kursusList as $kursus)
                                        <option value="{{ $kursus->id }}">{{ $kursus->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('kursus')<small class="text-danger">{{$message}}</small>@enderror
                        </div>

                        <div class="row mb-3">
                            <label for="instruktur" class="col-sm-2 col-form-label">Instruktur</label>
                            <div class="col-sm-10">
                                <input type="text" name="instruktur" class="form-control" id="instruktur">
                            </div>
                            @error('instruktur')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="kapasitas" class="col-sm-2 col-form-label">Kapasitas</label>
                            <div class="col-sm-10">
                                <input type="number" name="kapasitas" class="form-control" id="kapasitas" min="0">
                            </div>
                            @error('kapasitas')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
                            <div class="col-sm-10">
                                <div class="input-group mb-3">
                                    <span class="input-group-text">Rp</span>
                                    <input name="harga" type="text" class="form-control"
                                           aria-label="Amount (to the nearest rupiah)">
                                    <span class="input-group-text">.00</span>
                                </div>
                                {{--                                <input type="text" name="" class="form-control" id="inputEmail3">--}}
                            </div>
                            @error('gambar')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-2 col-form-label">Jadwal</div>
                            <div class="col-sm-10">
                                <div>
                                    <label for="hari" class="col-form-label">Hari</label>
                                    <select class="form-select" name="hari" aria-label="Default select example">
                                        <option selected>Pilih hari</option>
                                        <option value="0">Senin</option>
                                        <option value="1">Selasa</option>
                                        <option value="2">Rabu</option>
                                        <option value="3">Kamis</option>
                                        <option value="4">Jumat</option>
                                        <option value="5">Sabtu</option>
                                        <option value="6">Minggu</option>
                                    </select>
                                </div>
                                <span class="me-3">
                                    <label for="jam_mulai" class="col-form-label me-1">Jam Mulai</label>
                                    <input type="time" name="jam_mulai" id="">
                                </span>
                                <span>
                                    <label for="jam_selesai" class="col-form-label me-1">Jam Selesai</label>
                                    <input type="time" name="jam_selesai" id="">
                                </span>
                            </div>
                            @error('gambar')<small class="text-danger">{{$message}}</small>@enderror
                        </div>

                        <div class="col-md-12 mb-3">
                            <button type="submit" class="btn btn-primary float-end text-white">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
