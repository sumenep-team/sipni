@extends('layouts.sanggar')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4>Add Category
                        <a href="{{ url('sanggar/category/create') }}"
                           class="btn btn-primary btn-sm float-end text-white">BACK</a> {{--Ini bisa diganti return atau lainnya kek di mockup--}}
                    </h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('sanggar/category') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-6 mb-3">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control" id=""/>
                            @error('name')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Slug</label>
                            <input type="text" name="slug" class="form-control" id=""/>
                            @error('slug')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="">Description</label>
                            <textarea name="description" class="form-control" id="" rows="3"></textarea>
                            @error('description')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Image</label>
                            <input type="file" name="image" class="form-control" id=""/>
                            @error('image')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Status</label>
                            <input type="checkbox" name="status" id=""/>
                            @error('status')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12">
                            <h4>SEO Tags</h4>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Meta Title</label>
                            <input type="text" name="meta_title" class="form-control" id=""/>
                            @error('meta_title')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="">Meta Keyword</label>
                            <textarea name="meta_keyword" class="form-control" id="" rows="3"></textarea>
                            @error('meta_keyword')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="">Meta Description</label>
                            <textarea name="meta_description" class="form-control" id="" rows="3"></textarea>
                            @error('meta_description')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <button type="submit" class="btn btn-primary float-end">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
