@extends('layouts.sanggar')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-header">
                    <h4>Edit Category
                        <a href="{{ url('sanggar/category') }}"
                           class="btn btn-primary btn-sm float-end text-white">BACK</a> {{--Ini bisa diganti return atau lainnya kek di mockup--}}
                    </h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('sanggar/category/'.$category->id) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="col-md-6 mb-3">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control" id="" value="{{ $category->name }}"/>
                            @error('name')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Slug</label>
                            <input type="text" name="slug" class="form-control" value="{{ $category->slug }}" id=""/>
                            @error('slug')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="">Description</label>
                            <textarea name="description" class="form-control" id=""
                                      rows="3">{{ $category->description }}</textarea>
                            @error('description')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Image</label>
                            <input type="file" name="image" class="form-control" id=""/>
                            <img src="{{ asset('/uploads/category/' . $category->image) }}" width="60px" height="60px"
                                 alt="" srcset="">
                            @error('image')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Status</label>
                            <input type="checkbox" name="status" id="" {{ $category->status == '1' ? 'checked' : '' }}/>
                            @error('status')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12">
                            <h4>SEO Tags</h4>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="">Meta Title</label>
                            <input type="text" name="meta_title" class="form-control" id=""
                                   value="{{ $category->meta_title }}"/>
                            @error('meta_title')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="">Meta Keyword</label>
                            <textarea name="meta_keyword" class="form-control" id=""
                                      rows="3">{{ $category->meta_keyword }}</textarea>
                            @error('meta_keyword')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="">Meta Description</label>
                            <textarea name="meta_description" class="form-control" id=""
                                      rows="3">{{ $category->meta_description }}</textarea>
                            @error('meta_description')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <button type="submit" class="btn btn-primary float-end">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
